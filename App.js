import React from 'react';
import {Router,Scene,Drawer,Stack,Actions,CardStackStyleInterpolator} from 'react-native-router-flux';
import {Store} from './src/components/Store';
import {Provider, setStore} from 'trim-redux';
import { Icon } from 'native-base';
import {BackHandler, Alert, Linking, View, I18nManager, Image, AsyncStorage, Dimensions,DeviceEventEmitter} from 'react-native';
//اجبار به ltr کردن اپلیکیشن برای جلوگیری از همریختگی در گوشی های فارسی
I18nManager.allowRTL(false);

import Splash from "./src/Splash";
import DrawerLayout from "./src/DrawerLayout";
import Home from "./src/Home";
import Login from "./src/Login";
import Course from "./src/Course";
import Package from "./src/Package";
import Lesson from "./src/Lesson";
import Player from "./src/Player";
import OnlinePlayer from "./src/OnlinePlayer";
import Payment from "./src/Payment";
import Profile from "./src/Profile";
import Wallet from "./src/Wallet";
import Courses from "./src/Courses";
import AllCourses from "./src/AllCourses";
import WebViewer from "./src/WebViewer";
import Transactions from "./src/Transactions";
import FaText from "./src/components/FaText";
import {Setting} from "./src/components/Setting";
import Exam from "./src/Exam";
import Exams from "./src/Exams";
import Quiz from "./src/Quiz";
import { Root,StyleProvider } from "native-base";
import getTheme from './native-base-theme/components';
import platformTheme from './native-base-theme/variables/platform';
import QuizResult from "./src/QuizResult";
import Pushe from 'react-native-pushe'
import Dialog from "./src/components/Dialog";
import {DialogHelper} from "./src/components/DialogHelper";
import {alertMsg} from "./src/components/Helper";
import Favs from "./src/Favs";
import Message from "./src/Message";
import NoNet from "./src/NoNet";
import Intro from "./src/Intro";
import ProfileSteps from './src/ProfileSteps';
import OfflineVideos from "./src/OfflineVideos";


class TabIcon extends React.Component {
    render() {
        var color = this.props.focused ? Setting.primaryColor : Setting.ccc;
        var title = this.props.title;
        var icon = 'home';
        var thetitle = 'خانه';
        if(!this.props.focused) {
            if (title == 'home') {
                icon = 'home';
                thetitle = 'خانه';
                return (
                    <View style={{justifyContent: 'center', alignItems: 'center', margin: 15}}>
                        <Image resizeMode={'stretch'} source={require('./assets/images/tabs/home.png')}
                               style={{height: 25, width: 25}}/>
                    </View>
                );

            } else if (title == 'Courses') {
                icon = 'book';
                thetitle = 'دوره ها';
                return (
                    <View style={{justifyContent: 'center', alignItems: 'center', margin: 15}}>
                        <Image resizeMode={'stretch'} source={require('./assets/images/tabs/course.png')}
                               style={{height: 25, width: 25}}/>
                    </View>
                );
            } else if (title == 'Exams') {
                icon = 'book-reader';
                thetitle = 'دوره';
                return (
                    <View style={{justifyContent: 'center', alignItems: 'center', margin: 15}}>
                        <Image resizeMode={'stretch'} source={require('./assets/images/tabs/exam.png')}
                               style={{height: 25, width: 25}}/>
                    </View>
                );
            } else if (title == 'Profile') {
                icon = 'address-book';
                thetitle = 'آزمون';
                return (
                    <View style={{justifyContent: 'center', alignItems: 'center', margin: 15}}>
                        <Image resizeMode={'stretch'} source={require('./assets/images/tabs/user.png')}
                               style={{height: 25, width: 25}}/>
                    </View>
                );
            }
        }else{
            if (title == 'home') {
                icon = 'home';
                thetitle = 'خانه';
                return (
                    <View style={{justifyContent: 'center', alignItems: 'center', margin: 15}}>
                        <Image resizeMode={'stretch'} source={require('./assets/images/tabs/home1.png')}
                               style={{height: 25, width: 25}}/>
                    </View>
                );

            } else if (title == 'Courses') {
                icon = 'book';
                thetitle = 'دوره ها';
                return (
                    <View style={{justifyContent: 'center', alignItems: 'center', margin: 15}}>
                        <Image resizeMode={'stretch'} source={require('./assets/images/tabs/course1.png')}
                               style={{height: 25, width: 25}}/>
                    </View>
                );
            } else if (title == 'Exams') {
                icon = 'book-reader';
                thetitle = 'دوره';
                return (
                    <View style={{justifyContent: 'center', alignItems: 'center', margin: 15}}>
                        <Image resizeMode={'stretch'} source={require('./assets/images/tabs/exam1.png')}
                               style={{height: 25, width: 25}}/>
                    </View>
                );
            } else if (title == 'Profile') {
                icon = 'address-book';
                thetitle = 'آزمون';
                return (
                    <View style={{justifyContent: 'center', alignItems: 'center', margin: 15}}>
                        <Image resizeMode={'stretch'} source={require('./assets/images/tabs/user1.png')}
                               style={{height: 25, width: 25}}/>
                    </View>
                );
            }
        }

    }
}

export default class App extends React.Component {

    componentDidMount() {
        Pushe.initialize(true);

        //تعریف لیسنر برای شناسایی لینک های بازگشت از درگاه پرداخت
        Linking.addEventListener('url', this.handleOpenURL);
        Linking.getInitialURL().then((url) => {
            if (url) {
                // انجام فرآیند بالا در صورتی که اپلیکیشن به صورت کامل بسته باشه و استارت بوخوره
                this.handleOpenURL({url:url},showed);
            }
        }).catch(err => console.error('An error occurred', err));
    }

    componentWillUnmount() {
        //حذف لیسنر برای جلوگیری از کاهش سرعت اپ
        Linking.removeEventListener('url', this.handleOpenURL);
    }
    async handleOpenURL(event) {
        console.log(event);
        //تحلیل آدرس بازگشتی
        //اگر ۲ => ناموفق
        // اگر ۱ موفق خرید دوره
        // اگر ۳ موفق شارژ حساب کاربری

        if(event.url == 'nejateman://App/2'){
            setTimeout(()=>{
                alertMsg("پرداخت ناموفق","متاسفانه پرداخت شما ناموفق بود . درصورت کسر وجه از حساب شما لطفا با پشتیبانی تماس بگیرید");
            },100)
        }else if(event.url == 'nejateman://App/1'){

            setTimeout(()=>{
                alertMsg("پرداخت موفق","پرداخت شما با موفقیت انجام شد و دوره مورد نظر به لیست دوره های شما اضافه شد .\n با تشکر از خرید شما");
                return Actions.Courses();
            },100)

        }else if(event.url == 'nejateman://App/3'){
            setTimeout(()=>{
                alertMsg("پرداخت موفق","پرداخت شما با موفقیت انجام شد و حساب کاربری شما با موفقیت شارژ شد");
                return Actions.refresh({reloadthis:true});
            },100)

        }

    }


    render() {
        const transitionConfig = () => ({
            transitionSpec: {
                duration: 0
            },
        });
        var {height, width} = Dimensions.get('window');
        return (
            // پرووایدر قالب نیتیو بیس
            <StyleProvider style={getTheme(platformTheme)}>
                {/*روت اصلی اپلیکیشن برای نیتیو بیس*/}
                <Root >
            {/*//تعریف پرووایدر برای ریداکس*/}
                    <Provider store={Store}>
                        {/*روتر اصلی اپلیکیشن*/}
                        <Router backAndroidHandler={() => {return this.onBackPress()} } transitionConfig={transitionConfig}>
                            <Scene hideNavBar>
                                {/*صفحات اولیه اسپلش و لاگین که نیازی به دراور لایوت ندارند*/}
                                <Scene key="splash"  component={Splash}  initial />
                                <Scene key="login"  component={Login}   />
                                <Scene key="Quiz"  component={Quiz}   />
                                <Scene key="NoNet"  component={NoNet}   />
                                <Scene key="OfflineVideos"  component={OfflineVideos}   />
                                <Scene key="DupPlayer"  component={Player}   />
                                <Scene key="intro"  component={Intro}   />

                                <Scene key="main" hideNavBar>
                                    {/*منوی کناری*/}
                                    <Drawer key="drawer" contentComponent={DrawerLayout} drawerWidth={width * 0.80} drawerPosition="right">
                                        <Scene key="root"  hideNavBar>
                                            {/*تب ها*/}
                                            <Scene key="rootTab" tabs hideNavBar lazy showLabel={false} tabBarStyle={{height:50,backgroundColor:'#e9e9e9'}} >
                                                {/*<Scene key="Profile" title={"Profile"}  component={Profile} hideNavBar icon={TabIcon}  />*/}
                                                <Scene key="Profile" title={"Profile"}  component={ProfileSteps} hideNavBar icon={TabIcon}  />
                                                <Scene key="Exams" title={"Exams"}  component={Exams} hideNavBar icon={TabIcon} />
                                                <Scene key="Courses" title={"Courses"} onEnter={Courses.onEnterView}  component={Courses}  hideNavBar icon={TabIcon}  />
                                                <Scene key="home" title={"home"} gesturesEnabled={false} panHandlers={null} component={Home} hideNavBar   icon={TabIcon} initial />
                                            </Scene>

                                            <Stack key="root1"  hideNavBar>
                                                {/*سایر صفحات اپلیکیشن*/}
                                                <Scene key="Lesson" component={Lesson}  />
                                                <Scene key="Course" component={Course}  />
                                                <Scene key="Package" component={Package}  />
                                                <Scene key="Player"  component={Player}   />
                                                <Scene key="OnlinePlayer"  component={OnlinePlayer}   />
                                                <Scene key="Payment"  component={Payment}   />

                                                <Scene key="Wallet"  component={Wallet}   />
                                                <Scene key="Exam"  component={Exam}   />

                                                <Scene key="AllCourses"  component={AllCourses}   />
                                                <Scene key="WebViewer"  component={WebViewer}   />
                                                <Scene key="Transactions"  component={Transactions}   />
                                                <Scene key="QuizResult"  component={QuizResult}   />
                                                <Scene key="Favs" component={Favs}   />
                                                <Scene key="Message" component={Message}   />
                                            </Stack>


                                        </Scene>
                                    </Drawer>
                                </Scene>
                            </Scene>
                        </Router>
                        <Dialog ref={ref=>DialogHelper.setDialog(ref)} />

                    </Provider>
                </Root>
            </StyleProvider>
        )
    }
    onBackPress = () => {
        //کنترل دکمه بک سخت افزاری اندروید
        // در صورتی که در صفحه اصلی هست از کاربر سوال میکند برای خروج و در غیر این صورت صفحه مورد نظر را میبندد
        if (Actions.currentScene === '_home') {
            //بستن منوی کناری
            Actions.drawerClose();
            alertMsg(
                'خروج از سامانه',
                'آیا برای خروج از اپلیکیشن مطمین هستید ؟',
                [
                    {text: 'نه', onPress: () => {}},
                    {text: 'بله', onPress: () => BackHandler.exitApp()},
                ],
                {cancelable: true},
            );
            return true;

        }else {
            Actions.pop();
            return true;
        }
    }
}
