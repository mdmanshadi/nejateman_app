import React from 'react';
import {View, ScrollView, Dimensions,Image,FlatList,TouchableOpacity,Alert,AppState} from 'react-native';
import {connect, setStore} from "trim-redux";

import Video from 'react-native-video';
import Loading from "./components/Loading";
import {Actions} from "react-native-router-flux";
import MyButton from "./components/MyButton";
import Header from "./components/Header";
import Pdf from 'react-native-pdf';
import FaText from "./components/FaText";
import {Setting} from "./components/Setting";



var RNFS = require('react-native-fs');
var TheFilePath = RNFS.DocumentDirectoryPath+'/files/file';

export default class Player extends React.Component {

    componentWillMount(){
        this.setState({loading:true});
        TheFilePath = RNFS.DocumentDirectoryPath+'/files/file.'+this.props.ext;
        this.make(this.props.path);

        AppState.addEventListener('change', this._handleAppStateChange);
    }
    componentWillUnmount(){
        RNFS.unlink(TheFilePath);
        AppState.removeEventListener('change', this._handleAppStateChange);
    }
    _handleAppStateChange = async (nextAppState) => {
        if (nextAppState == 'inactive' || nextAppState == 'background'){
            await RNFS.unlink(TheFilePath);
            Actions.pop();
        }
    };
    async make(path){
        //var file = await RNFS.readFile(this.props.path,"base64");
        //var file = decrypt('OghiyaNoosEAbi1234567890', file);
        //var wfile = await RNFS.writeFile(TheFilePath, file, 'base64');
        RNFS.copyFile(this.props.path,TheFilePath);
        this.setState({loading:false})
    }
    renderData(){
        if(this.props.typee == 'video'){
            return(
                <Video source={{uri: '//' + TheFilePath}}
                       fullscreenOrientation={"portrait"}
                       fullscreenAutorotate={false}
                       controls={true}
                       fullscreen={true}
                       style={{marginTop:90,width:'100%',height:'60%'}}
                       onBuffer={this.onBuffer}
                       onError={(res) => {}}/>
            )
        }else if(this.props.typee == 'pdf'){
            return(
                <Pdf
                source={{uri: '//' + TheFilePath}}
                onLoadComplete={(numberOfPages,filePath)=>{
                }}
                onPageChanged={(page,numberOfPages)=>{
                }}
                onError={(error)=>{
                }}
                style={{marginTop:90,width:'100%',height:'80%',backgroundColor:'#fff'}}/>
            );
        }else{
            return(
                <View style={{marginTop:300}}>
                <FaText>این فرمت فایل پشتیبانی نمیشود.</FaText>
                    <MyButton>بازگشت</MyButton>
                </View>
            )
        }
    }
    render() {
        var {height, width} = Dimensions.get('window');

        return(
            <View style={{flex:1,backgroundColor:((this.props.typee == 'video') ? '#000' : '#fff')}}>
                <Header/>
                <Loading loading={this.state.loading}/>
                {!this.state.loading &&
                    this.renderData()
                }
            </View>
        )

    }

}
