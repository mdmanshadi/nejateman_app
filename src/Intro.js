import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {StyleSheet, View, Text, Image, Dimensions} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {Actions} from 'react-native-router-flux';


const slides = [
    {
        key: 'one',
        image: require('../assets/images/intro/1.jpg'),
    },
    {
        key: 'two',
        image: require('../assets/images/intro/2.jpg'),
    },
    {
        key: 'three',
        image: require('../assets/images/intro/3.jpg'),
    },
    {
        key: 'four',
        image: require('../assets/images/intro/4.jpg'),
    },
    {
        key: 'five',
        image: require('../assets/images/intro/5.jpg'),
    }
];


export default class Intro extends React.Component {
    _renderItem = ({item}) => {
        var {height, width} = Dimensions.get('window');
        return (
            <View style={{...styles.slide,backgroundColor:item.backgroundColor,height:height,width:width}}>
                <Text style={styles.title}>{item.title}</Text>
                <Image style={{height:height,width:width}} source={item.image}/>
                <Text style={styles.text}>{item.text}</Text>
            </View>
        );
    };
    _renderNextButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="md-arrow-round-forward"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };
    _renderDoneButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="md-checkmark"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };
    _onDone = () => {
        Actions.reset('main');
    };

    render() {
        return (
            <AppIntroSlider
                data={slides}
                renderDoneButton={this._renderDoneButton}
                renderNextButton={this._renderNextButton}
                renderItem={this._renderItem}
                onDone={this._onDone}
            />
        );
    }
}

const styles = StyleSheet.create({
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    slide: {
        justifyContent: 'center',
        alignItems: 'center',
    }
});
