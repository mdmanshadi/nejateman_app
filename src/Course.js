import React from 'react';
import {View, ScrollView, Dimensions, Image, FlatList, TouchableOpacity, WebView, ImageBackground} from 'react-native';
import {connect, getStore, setStore} from "trim-redux";
import FaText from "./components/FaText";
import {Setting,numberFormat} from "./components/Setting";
import {Actions} from 'react-native-router-flux';
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";
import MyButton from "./components/MyButton";
import Screen from "./components/Screen";
import {Spinner,Icon} from 'native-base';
import {alertMsg} from "./components/Helper";


export default class Course extends React.Component {

    componentWillMount(){
        this.setState({
            liked:false,
            stars:0,
            loading:true,
            data : {
                lessons : [],
                cats : [],
                description : '',
            },
        });
        Fetcher("getCategoryDetails",{id:this.props.id},true)
            .then((res)=>{
                this.setState({
                    loading:false,
                });
                if(res.status == 'OK'){
                    this.setState({
                        data : res.data,
                        liked : res.data.isFav == '1' ? true : false,
                        stars : res.data.stars || 0,
                    })
                }else{
                    Actions.reset("splash");
                }

            })
    }
    changeFav(){
        this.setState({loading:true});
        Fetcher("addFav",{id:this.props.id,type:'category'},true)
            .then((res)=>{
                this.setState({loading:false,liked:!this.state.liked});
            });
    }
    render() {
        var {height, width} = Dimensions.get('window');
        var starsOn = [];
        var starsOff = [];
        for (i = 0; i < parseInt(this.state.stars || 0); i++) {
            starsOn.push(i);
        }
        let remain = 5 - parseInt(this.state.stars || 0);
        for (i = 0; i < parseInt(remain); i++) {
            starsOff.push(i + starsOn.length);
        }

        return(

            <Screen title={this.state.data.title} >
                <Loading new loading={this.state.loading}/>

                <ImageBackground resizeMode={getStore('config').image_scale} source={{uri:this.state.data.pic}} style={{height:250}} >
                    {parseFloat(this.state.data.discount) > 0 &&
                    <ImageBackground resizeMode={'stretch'} style={{width:40,height:60,top:0,left:10,justifyContent:'flex-end',alignItems:'center'}} source={require('../assets/images/discountbg.png')} >
                        <FaText style={{bottom:5,color:Setting.white,fontSize:14}}>{Math.round(100 - ((parseFloat(this.state.data.price) - parseFloat(this.state.data.discount)) * 100) / parseFloat(this.state.data.price))}%</FaText>
                    </ImageBackground>
                    }
                </ImageBackground>

                <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:10,marginBottom:20}}>
                    <FaText border fontSize={18}>{this.state.data.title}</FaText>
                    <FaText html style={{marginTop:15,marginBottom:15}}>{this.state.data.description}</FaText>
                    {!this.state.loading &&
                    <View style={{padding:5,flexDirection:'row-reverse',justifyContent:'space-between',alignItems:'space-between'}}>
                        {parseFloat(this.state.data.discount) > 0 ?
                            <FaText style={{color: Setting.primaryColor, width: '49%',textDecorationLine: 'line-through'}} textAlign={'right'}>
                                {numberFormat(this.state.data.price)}
                            </FaText>
                            :
                            <View style={{color: Setting.white, width: '49%'}}></View>
                        }
                        <FaText style={{color:Setting.headerColor,width:'49%'}} textAlign={'left'}>
                            {numberFormat(parseFloat(this.state.data.price) - parseFloat(this.state.data.discount))}
                        </FaText>
                    </View>
                    }

                    <View style={{flexDirection:'row-reverse',justifyContent:'center'}}>
                        <View style={{width:'100%'}}>
                            {this.state.loading ?
                                <View></View>
                                :
                            this.state.data.owned == '1' ?
                                <MyButton bg={Setting.green} txtcolor={"#fff"} disable  style={{width:'100%'}}>خریداری شده</MyButton>
                                :
                                (this.state.data.for_sale == '1' ?
                                        <MyButton onPress={()=>Actions.Payment({
                                            product_name:this.state.data.title,
                                            price:parseFloat(parseFloat(this.state.data.price) ),
                                            discount:parseFloat(this.state.data.discount),
                                            total:parseFloat(this.state.data.price) - parseFloat(this.state.data.discount),
                                            score:parseFloat(this.state.data.score),
                                            id:this.state.data.id,
                                            typee:'course',
                                        })}  style={{width:'100%'}}>خرید این دوره</MyButton>
                                        :
                                        <MyButton disable  style={{width:'100%'}}>فروش غیرفعال</MyButton>
                                )
                            }
                            {this.state.data.hasExam == '1' &&
                            <MyButton disable={this.state.data.quizStatus == '1'} bg={this.state.data.quizStatus == '1' ? Setting.ccc : Setting.green} txtcolor={"#fff"} onPress={()=>Actions.Exam({id:this.state.data.id,typee:'course'})} style={{marginTop: 15, marginBottom: 15}}>شرکت در آزمون</MyButton>
                            }
                            <View style={{marginTop:15,flexDirection:'row-reverse',justifyContent:'space-between'}}>
                                <TouchableOpacity onPress={()=>this.changeFav()}>
                                {this.state.liked ?
                                    <Icon name={"heart"} style={{color:Setting.primaryColor}} type={"FontAwesome"}/>
                                    :
                                    <Icon name={"heart-o"} style={{color:Setting.primaryColor}} type={"FontAwesome"}/>
                                }
                                </TouchableOpacity>
                                <View style={{flexDirection:'row'}}>
                                    {starsOn.map((val,i)=>{
                                        return(<TouchableOpacity onPress={()=>this.setStar(val)}><Icon name={"star"} style={{color:'#fac917'}} type={"FontAwesome"}/></TouchableOpacity>)
                                    })}
                                    {starsOff.map((val,i)=>{
                                        return(<TouchableOpacity onPress={()=>this.setStar(val)}><Icon name={"star-o"} style={{color:'#999'}} type={"FontAwesome"}/></TouchableOpacity>)
                                    })}

                                </View>
                            </View>
                            {this.state.data.quizStatus == '1' &&
                            <FaText textAlign={"center"}>شما در آزمون این دوره شرکت کرده اید و با کسب امتیاز {this.state.data.quizScore} قبول شده اید .</FaText>
                            }
                        </View>
                    </View>
                    {this.state.data.lessons.length > 0 &&
                        <View>
                            <View style={{justifyContent:'center',alignItems:'center'}}>
                                <Image source={require('../assets/images/book.png')} style={{width:50,height:50,marginTop:20}}/>
                            </View>
                            <FaText fontSize={14} style={{marginTop:10,marginBottom:15,textAlign:'center'}}>دروس این دوره</FaText>

                            <FlatList
                                style={Setting.flatListStyle}
                                data={this.state.data.lessons}
                                renderItem={({item}) =>
                                    <TouchableOpacity  activeOpacity={1} onPress={()=>Actions.Lesson({id:item.id})} style={{padding:15,borderRadius:Setting.borderRadius,marginTop:10,flexDirection:'row-reverse',alignContent:'center',alignItems:'center',backgroundColor:Setting.f9}}>
                                        <View style={{justifyContent:'center',alignItems:'center',flexDirection:'row-reverse'}}>
                                            <FaText style={{color:'#7f8aab',textAlign:'center',width:'100%'}}>{item.title}</FaText>
                                        </View>
                                    </TouchableOpacity>
                                }
                                keyExtractor={(item)=>item.id}
                            />
                        </View>
                    }
                    {this.state.data.cats.length > 0 &&
                    <View>
                        <View style={{justifyContent:'center',alignItems:'center'}}>
                            <Image source={require('../assets/images/book.png')} style={{width:50,height:50,marginTop:20}}/>
                        </View>
                        <FaText fontSize={14} style={{marginTop:10,marginBottom:15,textAlign:'center'}}>دوره های این دسته بندی</FaText>

                        <FlatList
                            style={Setting.flatListStyle}
                            data={this.state.data.cats}
                            renderItem={({item}) =>
                                <TouchableOpacity  activeOpacity={1} onPress={()=>Actions.Course({id:item.id})} style={{padding:15,borderRadius:Setting.borderRadius,marginTop:10,flexDirection:'row-reverse',alignContent:'center',alignItems:'center',backgroundColor:Setting.f9}}>
                                    <View style={{justifyContent:'center',alignItems:'center'}}>
                                        <FaText style={{color:'#7f8aab',textAlign:'center'}}>{item.title}</FaText>
                                    </View>

                                </TouchableOpacity>
                            }
                            keyExtractor={(item)=>item.id}
                        />
                    </View>
                    }
                </View>
            </Screen>
        )
    }
    setStar(star){
        alertMsg("ثبت امتیاز","آیا مطمین به ذخیره امتیاز هستید ؟",[
            {
                text: 'لغو', onPress: () => {
                }
            },
            {
                text: 'تایید', onPress: () => {
                    let realStar = star + 1;
                    this.setState({stars:realStar});
                    Fetcher("setStar",{type:'course',id:this.state.data.id,star:realStar},true);
                }
            },
        ]);

    }
}