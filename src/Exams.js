import React from 'react';
import {View, ScrollView, Dimensions,Image,FlatList,TouchableOpacity,Alert} from 'react-native';
import {connect, setStore} from "trim-redux";
import FaText from "./components/FaText";
import {Setting,numberFormat} from "./components/Setting";
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";

import {Actions} from "react-native-router-flux";
import Screen from "./components/Screen";

export default class Exams extends React.Component {

    componentWillMount(){
        this.setState({
            courses : [],
            loading:true,
        })
        this.loadData();
    }

    loadData(){
        Fetcher("getUserExams",{},true)
            .then((res)=>{
                if(res.status == 'OK'){
                    this.setState({
                        courses : res.data,
                        loading:false,
                    })
                }else{
                    Actions.reset("splash",{soft:true});
                }
            });
    }
    open(item){

            if (item.type == 'course')
                return Actions.QuizResult({id: item.id,typee:'course'});
            if (item.type == 'package')
                return Actions.QuizResult({id: item.id,typee:'package'});
            if (item.type == 'lesson')
                return Actions.QuizResult({id: item.id,typee:'lesson'});
    }
    type(item){
        if(item.type == 'course')
            return "دوره";
        if(item.type == 'package')
            return "پکیج";
        if(item.type == 'lesson')
            return "درس";
    }
    render() {
        var {height, width} = Dimensions.get('window');

        if(this.state.courses.length == 0)
            return(
                <Screen float alignItems={'center'} home>
                    <Loading new loading={this.state.loading}/>
                    <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:30,marginBottom:20,justifyContent:'center',alignItems:'center'}}>
                        <Image resizeMode={'stretch'} source={require('./../assets/images/bg/transaction.png')} style={{height:width/1.432,width:width,marginBottom:30}} />

                        {!this.state.loading &&
                            <View style={{backgroundColor:Setting.red,flexDirection:'row-reverse',alignItems:'center',justifyContent:'center',padding:10}}>
                                <Image source={require('../assets/images/drawer/error.png')} style={{width:30,height:30,marginLeft:8}}/>
                                <FaText style={{color:'#fff',fontSize:12}}>تاکنون در آزمونی ای شرکت نکرده اید.</FaText>
                            </View>
                        }

                    </View>
                </Screen>
            )
        else
            return(
                <Screen float alignItems={'center'} home>
                    <Loading new loading={this.state.loading}/>

                    <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:15,marginBottom:15,justifyContent:'center',alignItems:'center'}}>
                        <Image resizeMode={'stretch'} source={require('./../assets/images/bg/transaction.png')} style={{height:width/1.432,width:width}} />
                        <View style={{width:'100%'}}>
                            <FlatList
                                style={Setting.flatListStyle}
                                data={this.state.courses}
                                extraData={this.state}
                                renderItem={({item}) =>{
                                    return(
                                        <TouchableOpacity  activeOpacity={1} onPress={()=>Actions.replace("QuizResult",{id: item.id})} style={{padding:8,width:'98%',borderRadius:Setting.borderRadius,margin:5,flexDirection:'row-reverse',alignItems:'center',backgroundColor:Setting.f9,bordercolor:'#ccc'}}>
                                            <View style={{flexDirection:'row-reverse',justifyContent:'center',alignItems:'center',flex:1,padding:10}}>
                                                <View>
                                                    <FaText textAlign={'center'} marquee style={{color:Setting.primaryColor}}>{this.type(item)} - {item.titleName} ({item.created})</FaText>
                                                    <FaText textAlign={'center'}  style={{color:'#000'}}>وضعیت : {item.status}</FaText>
                                                    <FaText textAlign={'center'}  style={{color:'#000'}}>امتیاز  : {item.score}</FaText>
                                                </View>
                                            </View>
                                        </TouchableOpacity>

                                    )}
                                }
                                keyExtractor={(item)=>item.id}
                            />
                        </View>
                    </View>


                </Screen>
            )

    }

}
