import React from 'react';
import {AsyncStorage, Dimensions, Image, View} from "react-native";
import FaText from "./components/FaText";
import {Icon, Button} from "native-base";
import MyButton from "./components/MyButton";
import {Actions} from 'react-native-router-flux';
import {Setting} from "./components/Setting";

export default class NoNet extends React.Component {
    componentWillMount() {
    }

    render() {
        let {height, width} = Dimensions.get('window');
        return (
            <View style={{width: width, height: height, justifyContent: 'space-around', alignItems: 'center'}}>
                <View>
                    <Icon type={"FontAwesome"} name="wifi"
                          style={{fontSize: 150, textAlign: 'center', color: Setting.primaryColor}}/>
                    <FaText style={{fontSize: 25, textAlign: 'center', marginTop: '10%'}}>اتصال شبکه برقرار
                        نیست</FaText>
                    <FaText style={{fontSize: 14, textAlign: 'center'}}>لطفا از اتصال خود به اینترنت مطمین شوید</FaText>
                </View>
                <View style={{width: '60%'}}>
                    <MyButton style={{width: '100%', marginVertical: 20}}
                              onPress={() => Actions.reset('splash')}>سعی مجدد</MyButton>
                    <MyButton
                        onPress={() => Actions.OfflineVideos()}>مشاهده آفلاین ویدیوها</MyButton>
                </View>
            </View>
        )
    }
}
