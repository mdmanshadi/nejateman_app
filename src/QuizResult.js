import React from 'react';
import {View, ScrollView, Dimensions, Image, FlatList, TouchableOpacity, Alert, AppState,ImageBackground} from 'react-native';
import {connect, getStore} from "trim-redux";
import FaText from "./components/FaText";
import {Setting,numberFormat} from "./components/Setting";
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";
import MyButton from "./components/MyButton";
import Communications from "react-native-communications";
import TextField from "./components/TextField";
import Screen from "./components/Screen";
import MyButtonSelect from "./components/MyButtonSelect";
import CountDown from 'react-native-countdown-component';
import {Actions} from "react-native-router-flux";
import {toastMsg} from "./components/Helper";


class QuizResult extends React.Component {

    componentWillMount(){
        this.setState({
            truescore : 0,
            totalscore : 0,
            falsescore : 0,
            score : 0,
            status : '',
            min_score : 0,
            questions : [],
            loading:true,
        })
        this.getData();
    }
    getData = ()=>{
        Fetcher("getExamResult",{id:this.props.id},true)
            .then((res)=>{
                if(res.status == 'OK'){
                    this.setState({
                        truescore : res.data.true,
                        falsescore : res.data.false,
                        totalscore : res.data.total,
                        status : res.data.status,
                        score : res.data.score,
                        min_score : res.data.min_score,
                        questions : res.data.questions,
                        loading:false,
                    });

                }else{
                    Actions.pop();
                    toastMsg(res.data);
                }
            })
    }
    render() {
        var {height, width} = Dimensions.get('window');


        return(

            <Screen title={"نتیجه آزمون"} float>

                <Loading new loading={this.state.loading}/>
                <View style={{marginRight:5,marginLeft:5,marginTop:5,marginBottom:20,justifyContent:'center',alignItems:'center'}}>
                    <Image resizeMode={'stretch'} source={require('./../assets/images/bg/header.png')} style={{height:width * (179/500),width:width}} />

                    <FaText style={{fontSize:15,color:Setting.red}}>نتیجه آزمون</FaText>
                    {!this.state.loading &&
                        <View style={{margin:8,borderColor:Setting.white,borderWidth:1,padding:20,width:'90%',borderRadius:5,flexDirection:'column'}}>

                        <View style={{width:'100%',backgroundColor:Setting.f9,padding:10}}>
                            <FaText style={{marginBottom:15,fontSize:12}}>تعداد پاسخ های صحیح : {this.state.truescore} امتیاز</FaText>
                            <FaText style={{marginBottom:15,fontSize:12}}>تعداد پاسخ های غلط : {this.state.falsescore} امتیاز</FaText>
                            <FaText style={{marginBottom:15,fontSize:12}}>تعداد کل سوالات : {this.state.totalscore} امتیاز</FaText>
                            <FaText style={{marginBottom:15,fontSize:12}}>نمره دریافتی شما از این آزمون : {this.state.score} امتیاز</FaText>
                            <FaText style={{marginBottom:15,fontSize:12}}>حداقل امتیاز برای قبولی : {this.state.min_score} امتیاز</FaText>


                        </View>
                        <View style={{width:'100%',backgroundColor:Setting.white}}>

                        <FaText textAlign={'center'} style={{marginBottom:15,color:Setting.green,fontSize:25}}>وضعیت :  {this.state.status}</FaText>

                            <FaText style={{marginTop:5,marginBottom:5,fontSize:10}}>{getStore('config').exam_end_text}</FaText>
                            <FaText textAlign={'center'} style={{marginBottom:15}}>میتوانید گزارش آزمون های خود را در منوی آزمون های من مشاهده کنید و در صورت قبولی مدرک قبولی خود را دریافت کنید.</FaText>
                            {/*{this.state.questions.map((val,i)=>{*/}
                                {/*return(*/}
                                    {/*<View style={{borderTopWidth:1,borderTopColor:Setting.primaryColor,padding:10}}>*/}
                                        {/*<FaText>- {val.title}</FaText>*/}
                                        {/*<View style={{backgroundColor:Setting.f9,padding:10}}>*/}
                                        {/*{val.answers.map((v1,i)=>{*/}
                                            {/*if(v1.user_select == '1' && v1.is_true == '1')*/}
                                                {/*return(*/}
                                                    {/*<FaText style={{color:Setting.success}}>{v1.title} (صحیح)</FaText>*/}
                                                {/*);*/}
                                            {/*else if(v1.is_true == '1')*/}
                                                {/*return(*/}
                                                    {/*<FaText style={{color:Setting.success}}>{v1.title}  (انتخاب صحیح)</FaText>*/}
                                                {/*);*/}
                                            {/*else if(v1.user_select == '1')*/}
                                                {/*return(*/}
                                                    {/*<FaText style={{color:Setting.red}}>{v1.title} (انتخاب اشتباه کاربر)</FaText>*/}
                                                {/*);*/}
                                            {/*else*/}
                                                {/*return(*/}
                                                    {/*<FaText style={{color:Setting.headerColor}}>{v1.title}</FaText>*/}
                                                {/*);*/}
                                                {/*})}*/}
                                        {/*</View>*/}
                                    {/*</View>*/}
                                {/*)*/}
                            {/*})}*/}
                            <MyButton onPress={()=>Actions.reset("splash",{soft:true})} style={{marginTop:30,width:'100%'}}>بازگشت</MyButton>
                        </View>
                    </View>
                    }
                </View>
            </Screen>
        )

    }

}
const mapStateToProps = (state) => {
    return {
        homeData : state.homeData

    }
};
export default connect(mapStateToProps)(QuizResult);

