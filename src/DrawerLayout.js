import React from 'react';
import {
    View,
    Image,
    TouchableOpacity,
    ImageBackground,
    AsyncStorage,
    Alert,
    BackHandler,
    ScrollView,
    Share
} from 'react-native';
import {Icon} from 'native-base';
import FaText from "./components/FaText";
import {Actions} from "react-native-router-flux";
import Communications from "react-native-communications";

import {getStore,connect} from 'trim-redux'
import {alertMsg} from "./components/Helper";
import {Setting} from "./components/Setting";



class DrawerLayout extends React.Component {

    route(name,data = {}) {
        Actions.drawerClose();
        Actions.replace(name, data);
    };

    render() {
        var homeData = this.props.homeData;
        return (
            <View style={{flex:1}}>
                <View style={{height:140,marginBottom:7,alignItems:'center',justifyContent:'center'}}>
                    <ImageBackground style={{backgroundColor:'#fff',alignItems:'center',justifyContent:'center',width:'100%',height:'100%'}} resizeMode={"cover"} source={require('./../assets/images/slider.png')}/>
                    {/*<FaText style={{fontSize:10}}>{this.props.homeData.user.name}</FaText>*/}
                    {/*<FaText style={{fontSize:10}}>اعتبار : {this.props.homeData.user.credit_text}</FaText>*/}
                </View>
                <View style={{flex:7,padding:7,marginRight:25}}>
                <ScrollView>
                    <TouchableOpacity  activeOpacity={1} onPress={()=>{Actions.drawerClose();Actions.Profile()}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/profile.png')}/>
                        <FaText style={{color:'#8d8d8d'}}>پروفایل کاربری</FaText>
                    </TouchableOpacity>
                    <TouchableOpacity  activeOpacity={1} onPress={()=>{this.route("Favs")}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/heart.png')}/>
                        <FaText style={{color:'#8d8d8d'}}>علاقه مندی ها</FaText>
                    </TouchableOpacity>
                    <TouchableOpacity  activeOpacity={1} onPress={()=>{this.route("Message")}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>

                        <ImageBackground resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/message.png')}>
                            {this.props.unreadMsg.toString() != '0' &&
                                <View style={{width:10,height:10,borderRadius:20,backgroundColor:Setting.primaryColor,justifyContent:'center',alignItems:'center',position:'absolute',top:-6,left:-6}}>
                                    <FaText style={{fontSize:5,color:'#fff'}}>{parseInt(this.props.unreadMsg) ?? 0 }</FaText>
                                </View>
                            }
                        </ImageBackground>
                        <FaText style={{color:'#8d8d8d'}}>صندوق پیام</FaText>
                    </TouchableOpacity>

                        <TouchableOpacity  activeOpacity={1} onPress={()=>{this.route('AllCourses')}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                            <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/all.png')}/>
                            <FaText style={{color:'#8d8d8d'}}>همه محصولات</FaText>
                        </TouchableOpacity>

                        <TouchableOpacity  activeOpacity={1} onPress={()=>{this.route("Wallet")}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                            <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/wallet.png')}/>
                            <FaText style={{color:'#8d8d8d'}}>کیف پول</FaText>
                        </TouchableOpacity>
                        <TouchableOpacity  activeOpacity={1} onPress={()=>{Actions.drawerClose();Actions.Courses()}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                            <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/course.png')}/>
                            <FaText style={{color:'#8d8d8d'}}>دوره های من</FaText>
                        </TouchableOpacity>
                    <TouchableOpacity  activeOpacity={1} onPress={()=>{Actions.drawerClose();Actions.OfflineVideos()}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/download.png')}/>
                        <FaText style={{color:'#8d8d8d'}}>دانلودهای من</FaText>
                    </TouchableOpacity>
                        <TouchableOpacity  activeOpacity={1} onPress={()=>{this.route("Transactions")}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                            <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/transactions.png')}/>
                            <FaText style={{color:'#8d8d8d'}}>تراکنش های من</FaText>
                        </TouchableOpacity>
                        {homeData.statics.map((val,i)=>{
                            if(val.icon == '')
                                return(
                                    <TouchableOpacity  activeOpacity={1} onPress={()=>{this.route("WebViewer",{link : val.link,title:val.title})}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                                        <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/all.png')}/>
                                        <FaText style={{color:'#8d8d8d'}}>{val.title}</FaText>
                                    </TouchableOpacity>
                                )
                            else
                                return(
                                    <TouchableOpacity  activeOpacity={1} onPress={()=>{this.route("WebViewer",{link : val.link,title:val.title})}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                                        <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={{uri:val.icon}}/>
                                        <FaText style={{color:'#8d8d8d'}}>{val.title}</FaText>
                                    </TouchableOpacity>
                                );

                        })}
                    <TouchableOpacity  activeOpacity={1} onPress={()=>{
                        Share.share(
                            {
                                message:
                                getStore("config").invite_text+'\nکدمعرف : '+this.props.homeData.user.id

                            }).catch(errorMsg => {});
                    }} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/share.png')}/>
                        <FaText style={{color:'#8d8d8d'}}>معرفی به دوستان</FaText>
                    </TouchableOpacity>
                        <TouchableOpacity  activeOpacity={1} onPress={()=>{alertMsg(
                            'خروج از سامانه',
                            'آیا برای خروج از سامانه مطمین هستید ؟ ',
                            [
                                {text: 'نه', onPress: () => {}},
                                {text: 'بله', onPress: () => this.removeToken()},
                            ],
                            {cancelable: false},
                        );}} style={{flexDirection:'row-reverse',alignItems:'center',margin:8}}>
                            <Image resizeMode={'contain'} style={{width: 20, height: 20,marginLeft:8}} source={require('./../assets/images/drawer/logout.png')}/>
                            <FaText style={{color:'#8d8d8d'}}>خروج از سامانه</FaText>
                        </TouchableOpacity>

                </ScrollView>
                    <View style={{justifyContent:'center',alignItems:'center',position:'absolute',bottom:0,right:0,width:'100%'}}>
                        <View style={{width:'70%',justifyContent:'center',alignItems:'center',marginTop:10,paddingTop:15,borderTopWidth:0.5,borderTopColor:'#8d8d8d'}}>
                            <View style={{justifyContent:'center',alignItems:'center',flexDirection:'row-reverse'}}>
                                <TouchableOpacity onPress={()=>Communications.web(getStore('config').instagram)} activeOpacity={0.8}><Image resizeMode={'contain'} style={{width: 20, height: 20,margin:8}} source={require('./../assets/images/drawer/social/instagram.png')}/></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Communications.web(getStore('config').aparat)} activeOpacity={0.8}><Image resizeMode={'contain'} style={{width: 20, height: 20,margin:8}} source={require('./../assets/images/drawer/social/aparat.png')}/></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Communications.web(getStore('config').sorosh)} activeOpacity={0.8}><Image resizeMode={'contain'} style={{width: 20, height: 20,margin:8}} source={require('./../assets/images/drawer/social/sorosh.png')}/></TouchableOpacity>

                            </View>
                            <View style={{justifyContent:'center',alignItems:'center',flexDirection:'row-reverse'}}>
                                <TouchableOpacity onPress={()=>Communications.web(getStore('config').whatsapp)} activeOpacity={0.8}><Image resizeMode={'contain'} style={{width: 20, height: 20,margin:8}} source={require('./../assets/images/drawer/social/whatsapp.png')}/></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Communications.web(getStore('config').linkedin)} activeOpacity={0.8}><Image resizeMode={'contain'} style={{width: 20, height: 20,margin:8}} source={require('./../assets/images/drawer/social/linkedin.png')}/></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Communications.web(getStore('config').telegram)} activeOpacity={0.8}><Image resizeMode={'contain'} style={{width: 20, height: 20,margin:8}} source={require('./../assets/images/drawer/social/telegram.png')}/></TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
    removeToken(){
        AsyncStorage.removeItem("token");
        Actions.reset("splash");
    }
}
const mapStateToProps = (state) => {
    return {
        homeData : state.homeData,
        unreadMsg : state.unreadMsg

    }
};
export default connect(mapStateToProps)(DrawerLayout);
