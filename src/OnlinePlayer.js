import React from 'react';
import { WebView } from 'react-native-webview';
import Orientation from 'react-native-orientation';

export default class OnlinePlayer extends React.Component {
    componentWillMount() {
        Orientation.unlockAllOrientations()
    }
    componentWillUnmount() {
        Orientation.lockToPortrait();
    }

    render() {
        return (
            <WebView
                originWhitelist={['*']}
                source={{ html: `<style>.h_iframe-aparat_embed_frame{position:relative;}.h_iframe-aparat_embed_frame .ratio{display:block;width:100%;height:auto;}.h_iframe-aparat_embed_frame iframe{position:absolute;top:0;left:0;width:100%;height:100%;}</style><div class="h_iframe-aparat_embed_frame"><span style="display: block;padding-top: 57%"></span><iframe src="https://www.aparat.com/video/video/embed/videohash/uzONI/vt/frame" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe></div>` }}
            />
        );
    }

}
