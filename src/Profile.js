import React from 'react';
import {View, ScrollView, Dimensions,Image,FlatList,TouchableOpacity,Alert,AsyncStorage,ImageBackground} from 'react-native';
import {connect, getStore, setStore} from "trim-redux";
import FaText from "./components/FaText";
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";
import MyButton from "./components/MyButton";
import {Actions} from "react-native-router-flux";
import TextField from "./components/TextField";
import Screen from "./components/Screen";
import ImagePicker from 'react-native-image-picker';
import {Setting} from "./components/Setting";
import {toastMsg} from "./components/Helper";
import Province from './../assets/Province';
import RNPickerSelect from 'react-native-picker-select';
import Pick from "./components/Pick";


export default class Profile extends React.Component {

    componentWillMount(){
        this.setState({
            loading:true,
            pic:'pic.png',

            name: '',
            parent_name: '',
            b_y: '',
            b_m: '',
            b_d: '',
            melicode: '',
            r_s: '',
            r_m: '',
            country: '',
            state: '',
            city: '',
            address: '',
            zipcode: '',
            email: '',
            phone: '',
            pic_personal:'',
            id:'',
            stateList : [],
            cityList : [],
        });
        this.loadData();
        this.loadStateList();
    }
    loadStateList(){
        let List = [];
        Province.map((val)=>{
            List.push({ label: val.name, value: val.name });
        });
        this.setState({
            stateList : List,
        })
    }
    loadCityList(value){
        let List = [];
        let CityList = Province.find(x=>x.name == value);
        if(CityList)
        CityList.Cities.map((val)=>{
            List.push({ label: val.name, value: val.name });
        });
        this.setState({
            cityList : List,
        })
    }
    loadData(){
        Fetcher("getUserProfile",{},true)
            .then((res)=>{
                if(res.status == 'OK'){
                    this.setState({
                        name : res.data.name,
                        parent_name: res.data.parent_name,
                        b_y: res.data.b_y,
                        b_m: res.data.b_m,
                        b_d: res.data.b_d,
                        melicode: res.data.melicode,
                        r_s: res.data.r_s,
                        r_m: res.data.r_m,
                        address: res.data.address,
                        zipcode: res.data.zipcode,
                        email: res.data.email,
                        phone: res.data.phone,
                        id : res.data.id,

                        loading:false,
                        pic:res.data.pic,
                    },()=>{
                        this.loadStateList();
                        this.setState({
                            country: res.data.country,
                        },()=>{
                            this.setState({
                                state: res.data.state,
                            },()=>{
                                this.loadCityList(res.data.state);
                                setTimeout(()=>{
                                    this.setState({
                                        city: res.data.city,
                                    })
                                },1500);
                            })
                        });
                    })
                }else{
                    Actions.reset("splash");
                }
            });
    }
    save(){
        this.setState({loading:true});
        Fetcher("saveUserProfile",{

            name : this.state.name,
            parent_name:  this.state.parent_name,
            b_y:  this.state.b_y,
            b_m:  this.state.b_m,
            b_d:  this.state.b_d,
            melicode:  this.state.melicode,
            r_s:  this.state.r_s,
            r_m:  this.state.r_m,
            country:  this.state.country,
            state:  this.state.state,
            city:  this.state.city,
            address:  this.state.address,
            zipcode:  this.state.zipcode,
            email:  this.state.email,
            phone:  this.state.phone,
        },true)
            .then((res)=>{
                this.setState({loading:false});
                if(res.status == 'OK'){
                    setTimeout(()=>toastMsg("اطلاعات با موفقیت ذخیره شد"),100);
                    Actions.reset("splash",{soft:true});
                }else{
                    toastMsg(res.data)
                }

            })
    }
    selectCameraImage(){
        const options = {
            title: 'انتخاب تصویر',
            takePhotoButtonTitle: 'گرفتن تصویر با دوربین',
            chooseFromLibraryButtonTitle: 'انتخاب عکس از گالری',
            cancelButtonTitle: 'انصراف',
            quality : 0.5,
            mediaType: 'photo',
            maxWidth:500,
            maxHeight:500,


            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.launchCamera(options, async (response) => {


            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                const source = { uri: response.uri };
                this.setState({loading:true});
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    pic: response.uri,
                    picURL: response.path,
                });

                var data = new FormData();

                data.append('avatar', {
                    uri: response.uri, // your file path string
                    name: 'avatar.jpg',
                    type: 'image/jpg'
                });

                Fetcher("ChangeAvatar",data,true,true)
                    .then(res=>{
                        this.setState({loading:false});
                        if(res.status == 'OK'){
                            toastMsg("تصویر پروفایل با موفقیت بارگزاری شد")
                        }else{
                            toastMsg("خطایی در بارگزاری تصویر پروفایل رخ داد")
                        }
                    });

            }
        });
    }
    selectGalleyImage(){
        const options = {
            title: 'انتخاب تصویر',
            takePhotoButtonTitle: 'گرفتن تصویر با دوربین',
            chooseFromLibraryButtonTitle: 'انتخاب عکس از گالری',
            cancelButtonTitle: 'انصراف',
            quality : 0.5,
            mediaType: 'photo',
            maxWidth:500,
            maxHeight:500,


            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.launchImageLibrary(options, async (response) => {


            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                console.log("response",response);

                const source = { uri: response.uri };
                this.setState({loading:true});
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    pic: response.uri,
                    picURL: response.path,
                });


                var data = new FormData();

                data.append('avatar', {
                    uri: response.uri, // your file path string
                    name: 'avatar.jpg',
                    type: 'image/jpg'
                });

                Fetcher("ChangeAvatar",data,true,true)
                    .then(res=>{
                        this.setState({loading:false});
                        if(res.status == 'OK'){
                            toastMsg("تصویر پروفایل با موفقیت بارگزاری شد")
                        }else{
                            toastMsg("خطایی در بارگزاری تصویر پروفایل رخ داد")
                        }
                    });
            }
        });
    }
    selectGalleyPersonalImage(){
        const options = {
            title: 'انتخاب تصویر',
            takePhotoButtonTitle: 'گرفتن تصویر با دوربین',
            chooseFromLibraryButtonTitle: 'انتخاب عکس از گالری',
            cancelButtonTitle: 'انصراف',
            quality : 0.5,
            mediaType: 'photo',
            maxWidth:500,
            maxHeight:500,


            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.launchImageLibrary(options, async (response) => {


            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                const source = { uri: response.uri };
                this.setState({loading:true});
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    pic_personal: response.uri,
                    pic_personalURL: response.path,
                });


                var data = new FormData();

                data.append('avatar', {
                    uri: response.uri, // your file path string
                    name: 'avatar.jpg',
                    type: 'image/jpg'
                });

                Fetcher("ChangePersonalPic",data,true,true)
                    .then(res=>{
                        this.setState({loading:false});
                        if(res.status == 'OK'){
                        }else{
                        }
                    });

            }
        });
    }
    render() {
        var {height, width} = Dimensions.get('window');
        return(
            <Screen float={true} home>
                <Loading new loading={this.state.loading} title={"پروفایل من"}/>
                <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:15,marginBottom:15,justifyContent:'center',alignItems:'center'}}>
                    <ImageBackground resizeMode={'stretch'} source={require('./../assets/images/bg/profile.png')} style={{height:width/1.432,width:width,justifyContent:'center',alignItems:'center',flexDirection:'column',marginBottom:15}} >

                        <View style={{justifyContent:'center',alignItems:'center',flexDirection:'row-reverse'}}>
                            <TouchableOpacity activeOpacity={0.8} onPress={()=>this.selectCameraImage()}>
                                <Image resizeMode={"cover"} source={require('./../assets/images/bg/camera.png')} style={{height:40,width:40,margin:10,borderRadius:20}} />
                            </TouchableOpacity>

                            <TouchableOpacity activeOpacity={1}>
                                <Image resizeMode={"cover"} source={{uri:this.state.pic}} style={{height:120,width:120,margin:10,borderRadius:60}} />
                            </TouchableOpacity>

                            <TouchableOpacity activeOpacity={0.8} onPress={()=>this.selectGalleyImage()}>
                                <Image resizeMode={"cover"} source={require('./../assets/images/bg/gallery.png')} style={{height:40,width:40,margin:10,borderRadius:20}} />
                            </TouchableOpacity>
                        </View>
                        <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
                            <FaText style={{fontSize:15}}>کد معرف : {this.state.id}</FaText>
                        </View>

                    </ImageBackground>

                    <TextField value={this.state.name} onChangeText={(value)=>this.setState({name:value})} placeholder={"نام و نام خانوادگی "}/>
                    <TextField value={this.state.parent_name} onChangeText={(value)=>this.setState({parent_name:value})} placeholder={"نام پدر"}/>
                    <View style={{flexDirection:'row-reverse',width:'100%',justifyContent:'space-between',alignItems:'center'}}>
                        <View style={{width:'59%'}}>
                            <TextField value={this.state.b_y} keyboardType={"numeric"} onChangeText={(value)=>this.setState({b_y:value})} maxLength={4} placeholder={"سال تولد"}/>
                        </View>
                        <FaText>/</FaText>
                        <View style={{width:'19%'}}>
                            <TextField value={this.state.b_m} keyboardType={"numeric"} onChangeText={(value)=>this.setState({b_m:value})} maxLength={2} placeholder={"ماه"}/>
                        </View>
                        <FaText>/</FaText>
                        <View style={{width:'19%'}}>
                            <TextField value={this.state.b_d} keyboardType={"numeric"} onChangeText={(value)=>this.setState({b_d:value})} maxLength={2} placeholder={"روز"}/>
                        </View>
                    </View>
                    <TextField value={this.state.melicode} keyboardType={"numeric"} onChangeText={(value)=>this.setState({melicode:value})} maxLength={10} placeholder={"کدملی"}/>
                    <View style={{flexDirection:'row-reverse',width:'100%',justifyContent:'space-between',alignItems:'center'}}>
                        <View style={{width:'49%'}}>
                            <TextField value={this.state.rs} editable={false}  placeholder={"سابقه رانندگی"}/>
                        </View>
                        <View style={{width:'25%'}}>
                            <TextField value={this.state.r_s} keyboardType={"numeric"} onChangeText={(value)=>this.setState({r_s:value})} maxLength={2} placeholder={"سال"}/>
                        </View>
                        <FaText>/</FaText>
                        <View style={{width:'23%'}}>
                            <TextField value={this.state.r_m} keyboardType={"numeric"} onChangeText={(value)=>this.setState({r_m:value})} maxLength={2} placeholder={"ماه"}/>
                        </View>
                    </View>
                    <View style={{flexDirection:'row-reverse',width:'100%',justifyContent:'space-between'}}>
                        <View style={{width:'90%'}}>
                            <TextField value={this.state.p} editable={false} placeholder={this.state.pic_personal == '' ? 'عکس پرسنلی جهت گواهی نامه' : 'انتخاب شده'}/>
                        </View>

                        <TouchableOpacity onPress={()=>{console.log("asdsd");this.selectGalleyPersonalImage()}} style={{width:'10%',padding:3,justifyContent:'center',alignItems:'center'}}>
                            <Image resizeMode={"contain"} source={require('./../assets/images/pick_img.png')} style={{width:20,height:20}}/>
                        </TouchableOpacity>
                    </View>


                    <View style={{width:'100%'}}>
                        <Pick
                        onValueChange={(value,label) => {if(value != 0) this.setState({country:value})}}
                        items={[
                            {
                                label: 'ایران',
                                value: 'ایران',
                                key: 'ایران',
                            }
                        ]}
                        value={this.state.country}
                        itemKey={this.state.country}
                        placeholder={'کشور'}
                        />
                    </View>

                    <View style={{flexDirection:'row-reverse',width:'100%',justifyContent:'space-between'}}>
                        <View style={{width:'49%'}}>
                            <Pick
                                onValueChange={(value,label) => {if(value != 0) {this.setState({state:value});this.loadCityList(value)}}}
                                items={this.state.stateList}
                                placeholder={'استان'}
                                itemKey={this.state.state}
                                value={this.state.state}
                            />
                        </View>
                        <View style={{width:'49%'}}>
                            <Pick
                                onValueChange={(value,label) => {if(value != 0) this.setState({city:value})}}
                                items={this.state.cityList}
                                placeholder={'شهر'}
                                itemKey={this.state.city}
                                value={this.state.city}
                            />
                        </View>
                    </View>
                    <TextField value={this.state.address} onChangeText={(value)=>this.setState({address:value})} placeholder={"آدرس"}/>
                    <TextField value={this.state.zipcode} keyboardType={"numeric"} onChangeText={(value)=>this.setState({zipcode:value})} maxLength={10} placeholder={"کدپستی"}/>
                    <TextField value={this.state.email}  onChangeText={(value)=>this.setState({email:value})} placeholder={"پست الکترونیک"}/>
                    <TextField value={this.state.phone} keyboardType={"numeric"} onChangeText={(value)=>this.setState({phone:value})} maxLength={12} placeholder={"شماره تلفن ثابت"}/>

                    <MyButton onPress={()=>this.save()}>ذخیره اطلاعات</MyButton>
                </View>
            </Screen>
        )

    }

}
