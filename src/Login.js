import React from 'react';
import {Image, View, PushNotificationIOS, Alert, KeyboardAvoidingView, AsyncStorage, ScrollView, PermissionsAndroid, Dimensions,TouchableOpacity} from 'react-native';
import MyButton from "./components/MyButton";
import {Fetcher} from "./components/Upload";
import TextField from "./components/TextField";
import Loading from "./components/Loading";
import {Actions} from "react-native-router-flux";
import FaText from "./components/FaText";
import SmsListener from 'react-native-android-sms-listener';
import LinearGradient from 'react-native-linear-gradient';
import {Setting} from "./components/Setting";
import CountDown from 'react-native-countdown-component';
import {toastMsg} from "./components/Helper";
import {getStore, setStore} from "trim-redux";




export default class Login extends React.Component {

    componentWillMount() {


        this.setState({
            loading:false,
            username:'',
            code:'',
            name:'',
            ref_code:'',
            step:true,
            sendAgain : true,
            rule1 : false,
            rule2 : false,
            sendAgainTime : 60,
        });
    }

    async smsListener(){
        var perm = await this.requestPermission('RECEIVE_SMS');

        let subscription = SmsListener.addListener(message => {
            var str = message.body;
            var patt1 = /\d{4}/g;
            var result = str.match(patt1);

            this.setState({
                code : result[0]
            });
            if(!this.state.new)
                this.login();



        })
    }
    async requestPermission(perm) {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS[perm],
                {
                    title: 'اجازه دسترسی به پیامک',
                    message:
                        'این نرم افزار برای انجام اعتبارسنجی به اجازه شما نیاز دارد',
                    buttonPositive: 'اجازه دسترسی',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                return true;
            } else {
                return false;
            }
        } catch (err) {
            return false;
        }
    }
    login(){
        if(this.state.username == '' || (!this.state.step && this.state.code == ''))
            return toastMsg("لطفا اطلاعات درخواستی را وارد کنید");

        if(this.state.step){
            this.setState({loading: true});
            this.smsListener();
            Fetcher("login", {username: this.state.username})
                .then(res => {
                    this.setState({loading: false});
                    if (res.status == 'OK') {
                        this.setState({step: false});
                        // toastMsg("کد فعال سازی به تلفن همراه شما ارسال شد.");

                        if(res.data == 'new')
                            this.setState({new: true});
                    } else {
                        setTimeout(()=>toastMsg(res.data),100);
                    }
                });
        }else {
            if((!this.state.rule1 || !this.state.rule2) && this.state.new)
                return  toastMsg("لطفا قوانین حریم خصوصی و سیاست محرمانگی را بپذیرید");
            this.setState({loading: true});
            Fetcher("login",
                {
                    username: this.state.username,
                    code: this.state.code,
                    name: this.state.name,
                    ref_code: this.state.ref_code,
                })
                .then(res => {
                    this.setState({loading: false});
                    if (res.status == 'OK') {
                        AsyncStorage.setItem("token",res.data)
                            .then(()=>{
                                return Actions.reset("splash");
                            });
                    } else {
                        setTimeout(()=>toastMsg(res.data),100);
                    }
                });
        }
        //setTimeout(()=>{Actions.replace("home")},100)
    }
    handleCode(value){
        var newStatus  = this.state.new;
        console.log("newStatus",newStatus);
        this.setState({code:value},()=>{
            console.log("newStatus",newStatus);
            if(value.length == 4 && !newStatus){
                setTimeout(()=>this.login(),100);
            }
        });


    }
    handleMobile(value){
        this.setState({username:value},()=>{
            if(value.length == 11){
                setTimeout(()=>this.login(),100);
            }
        });

    }
    render() {
        var {height, width} = Dimensions.get('window');

        return (
            <View style={{width:width,backgroundColor:'#fff',flex:1,alignItems:'center',alignContent:'center',paddingTop:'8%'}}>

                    <Loading loading={this.state.loading}/>
                    <Image resizeMode={'stretch'} style={{ width: width,height:width * 0.400,marginBottom:'5%'}} source={require('./../assets/images/logo_login.png')}/>
                    <FaText style={{color:Setting.headerColor,fontSize:18}}>ورود به سامانه</FaText>
                    {this.state.new &&
                    <FaText style={{color:Setting.headerColor}}>برای ثبت نام در سامانه لطفا اطلاعات دقیق خود را وارد کنید</FaText>
                    }
                    <ScrollView showsHorizontalScrollIndicator={false} keyboardDismissMode={"on-drag"} keyboardShouldPersistTaps={"always"} contentContainerStyle={{width:width,alignItems:'center',alignContent:'center'}} >
                        {this.state.step ?

                            <TextField maxLength={11} style={{width:'80%'}} keyboardType={"numeric"} value={this.state.username} onChangeText={(value)=>this.handleMobile(value)} placeholder={"تلفن همراه"}/>

                            :
                            <View style={{width:'100%',alignItems:'center',alignContent:'center'}}>


                                <View>
                                    {this.state.sendAgain ?
                                    <CountDown
                                        until={this.state.sendAgainTime}
                                        size={10}
                                        onFinish={() => this.setState({
                                            sendAgain : false,
                                        })}
                                        digitStyle={{backgroundColor: 'transparent', margin: 10,fontFamily:'IRANSansWeb(FaNum)',}}
                                        digitTxtStyle={{color: Setting.headerColor, fontSize: 12,fontFamily:'IRANSansWeb(FaNum)',}}
                                        timeToShow={['S']}
                                        timeLabels={{m: '', s: ''}}

                                    />
                                        :
                                    <TouchableOpacity  activeOpacity={1} style={{marginTop:20,color:Setting.headerColor}} onPress={()=>{
                                        this.setState({
                                            loading:false,
                                            code:'',
                                            step:true,
                                            sendAgainTime:0,
                                        },()=>{
                                            this.login();
                                        });

                                    }}>
                                        <FaText style={{color:Setting.headerColor}}>ارسال مجدد کل فعال سازی</FaText>
                                    </TouchableOpacity>
                                    }
                                </View>
                                <TextField  textContentType={"oneTimeCode"} style={{width:'80%'}} keyboardType={"numeric"} value={this.state.code} maxLength={4} onChangeText={(value)=>this.handleCode(value)} placeholder={"کدفعال سازی دریافتی از پیامک"}/>
                                {this.state.new &&
                                <View style={{width:'80%'}}>
                                    <TextField  value={this.state.name} onChangeText={(value)=>this.setState({name:value})} placeholder={"نام و نام خانوادگی شما"}/>
                                    <TextField  value={this.state.ref_code} onChangeText={(value)=>this.setState({ref_code:value})} placeholder={"کد معرف شما"}/>
                                    <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',alignItems:'center'}}>
                                        <TouchableOpacity style={{width:18,height:18,borderWidth:1,borderColor:Setting.headerColor,justifyContent:'center',alignItems:'center'}} onPress={()=>this.setState({rule1:!this.state.rule1})}>
                                            {this.state.rule1 &&
                                                <View style={{
                                                    width: 14,
                                                    height: 14,
                                                    backgroundColor: Setting.headerColor
                                                }}/>
                                            }
                                        </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={1} style={{padding:10}} onPress={()=>toastMsg(getStore('config').privacy)}>
                                        <FaText>حریم خصوصی</FaText>
                                    </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',alignItems:'center'}}>
                                        <TouchableOpacity style={{width:18,height:18,borderWidth:1,borderColor:Setting.headerColor,justifyContent:'center',alignItems:'center'}} onPress={()=>this.setState({rule2:!this.state.rule2})}>
                                            {this.state.rule2 &&
                                            <View style={{
                                                width: 14,
                                                height: 14,
                                                backgroundColor: Setting.headerColor
                                            }}/>
                                            }
                                        </TouchableOpacity>
                                        <TouchableOpacity activeOpacity={1} style={{padding:10}} onPress={()=>toastMsg(getStore('config').rules)}>
                                            <FaText>سیاست محرمانگی</FaText>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                }
                            </View>
                        }
                        <MyButton gradient txtcolor={Setting.white} style={{width:'80%',marginTop:30}} onPress={()=>this.login()}>ورود</MyButton>
                        {!this.state.step &&
                            <TouchableOpacity  activeOpacity={1} style={{marginTop:20}} onPress={()=>{
                                this.setState({
                                    loading:false,
                                    username:'',
                                    code:'',
                                    name:'',
                                    ref_code:'',
                                    step:true,
                                    new : false,
                                });
                            }}>
                                <FaText style={{color:Setting.headerColor}}>تلفن همراه صحیح نیست ؟</FaText>
                            </TouchableOpacity>
                        }
                    </ScrollView>
            </View>
        )
    }


}