import React from 'react';
import {View, ScrollView, Dimensions, Image, FlatList, TouchableOpacity, Alert, AppState} from 'react-native';
import {connect, setStore} from "trim-redux";
import FaText from "./components/FaText";
import {Setting,numberFormat} from "./components/Setting";
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";
import MyButton from "./components/MyButton";
import Communications from "react-native-communications";
import TextField from "./components/TextField";
import Screen from "./components/Screen";
import {Actions} from "react-native-router-flux";
import {alertMsg} from "./components/Helper";

class Wallet extends React.Component {

    componentWillMount(){
        this.setState({
            amount : this.props.amount || 0,
            loading : false,
        })
        AppState.addEventListener('change', this._handleAppStateChange);
    }
    componentWillUnmount(){
        AppState.removeEventListener('change', this._handleAppStateChange);
    }
    _handleAppStateChange = async (nextAppState) => {
        console.log(nextAppState);
        if (nextAppState == 'active' && !this.state.loading){
            this.loadSync();
        }
    };
    loadSync = async ()=>{
        console.log("eeeee");
        this.setState({loading:true});
        Fetcher("GetMain",{},true)
            .then((res)=>{
                setStore({
                    homeData : res.data,
                });
                this.setState({loading:false});

            });


    };
    async startPay(){
        if(this.state.amount == '')
            return alertMsg("خطا","لطفا مبلغ مورد نظر را وارد کنید");

        this.setState({loading:true});
        var res = await Fetcher("ChargeWallet",{amount:this.state.amount},true);
        this.setState({loading:false});
        if(res.status == 'OK'){
            Communications.web(res.data);
        }else{
            setTimeout(()=>alertMsg("خطا",res.data),100);
        }
    }
    render() {
        var {height, width} = Dimensions.get('window');

        return(

            <Screen float={true} title={"کیف پول"} >
                <Loading loading={this.state.loading}/>
                <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:15,marginBottom:15,justifyContent:'center',alignItems:'center'}}>
                    <Image resizeMode={'stretch'} source={require('./../assets/images/bg/wallet.png')} style={{height:width/1.432,width:width}} />
                    <View style={{borderColor:Setting.white,borderWidth:1,marginTop:20,width:'100%',borderRadius:Setting.borderRadius,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                        <FaText style={{width:'100%',backgroundColor:Setting.grey,color:'#fff',textAlign:'center',padding:10}}>موجودی فعلی : {numberFormat(this.props.homeData.user.credit)} </FaText>
                        <TextField placeholder={"شارژ حساب کاربری به تومان"} keyboardType={"numeric"} value={this.state.amount} onChangeText={(value)=>this.setState({amount:value})} />
                        <MyButton onPress={()=>this.startPay()} style={{marginTop:30}}>شارژ آنلاین</MyButton>
                    </View>
                </View>
            </Screen>
        )

    }

}
const mapStateToProps = (state) => {
    return {
        homeData : state.homeData
    }
};

export default connect(mapStateToProps)(Wallet);
