import React from 'react';
import {View, ScrollView, Dimensions, Image, FlatList, TouchableOpacity, Alert, ImageBackground} from 'react-native';
import {connect, getStore, setStore} from 'trim-redux';
import FaText from './components/FaText';
import Swiper from 'react-native-swiper';
import {Setting, numberFormat} from './components/Setting';
import {Actions} from 'react-native-router-flux';
import Loading from './components/Loading';
import {Fetcher} from './components/Upload';
import MyButton from './components/MyButton';
import Header from './components/Header';
import Screen from './components/Screen';
import {Icon} from 'native-base';
import {alertMsg} from './components/Helper';


var RNFS = require('react-native-fs');
var filesPath = RNFS.DocumentDirectoryPath + '/files';
var lessonPath = filesPath + '/L_';
export default class Lesson extends React.Component {

    componentWillMount() {
        lessonPath = filesPath + '/L_' + this.props.id;
        this.setState({
            loading: true,
            liked: false,
            downloadProgress: 0,
            stars: 0,
            downloadSize: '...',
            downloadedSize: 0,
            download: 0,
            data: {
                files: [],
                description: '',
            },
            files: [],
        });

        Fetcher('getLessonDetails', {id: this.props.id}, true)
            .then((res) => {
                console.log('res lesson details',res);
                if (res.status == 'OK') {
                    this.setState({
                        data: res.data,
                        liked: res.data.isFav == '1' ? true : false,
                        stars: res.data.stars || 0,

                    });
                    var files = res.data.files;
                    this.syncFiles(files);
                } else {
                    Actions.reset('splash');
                }

            });
    }

    async syncFiles(files) {
        let value = {};
        await Promise.all(
            files.map(async (val, i) => {

                var avail = await this.checkFile(val.id + '.lpg');
                val['avail'] = avail ? 'yes' : 'no';
            }),
        );

        setTimeout(() => {
            this.setState({
                files: files,
                avail: value,
                loading: false,
            });
            console.log('files',files);
        }, 1);


    }

    async processFile(id, file_id, ext, typee) {
        //check if exists


        await RNFS.mkdir(filesPath, {NSURLIsExcludedFromBackupKey: true});
        await RNFS.mkdir(lessonPath, {NSURLIsExcludedFromBackupKey: true});
        var check = await this.checkFile(file_id);


        if (this.state.data.owned != '1') {
            if (check) {
                this.deleteFile(file_id);
            }
            return alertMsg('خطا', 'لطفا ابتدا درس مورد نظر را خریداری کنید !');

        }

        if (!check) {
            if (this.state.download != 0) {
                return alertMsg('خطا', 'در حال حاضر دانلودی در جریان است . لطفا منتظر بمانید');
            }
            let videoButtons = [
                {text: 'دانلود فایل', onPress: () => this.downloadFile(id, file_id)},
                {text: 'مشاهده آنلاین', onPress: () => Actions.OnlinePlayer()},
            ];
            let pdfButtons = [
                {text: 'بازگشت', onPress: () => {}},
                {text: 'دانلود فایل', onPress: () => this.downloadFile(id, file_id)},
            ];
            //file not available
            alertMsg(
                'پیام',
                'محتوای مورد نظر در گوشی شما موجود نیست . لطفا یکی از گزینه های زیر را انتخاب کنید',
                typee === 'video' ? videoButtons : pdfButtons,
                {cancelable: false},
            );
        } else {
            let path = lessonPath + '/' + 'FILE_'+ file_id;
            Actions.Player({path: path, ext: ext, typee: typee});
        }
    }

    progress = (log) => {
        var percent = Math.round((log.bytesWritten * 100) / log.contentLength);
        this.setState({
            downloadProgress: percent,
            downloadSize: Math.round(log.contentLength / 1024 / 1024, 2),
            downloadedSize: Math.round(log.bytesWritten / 1024 / 1024, 2),
        });
    };

    async makeVideo(path) {
        // var file = await RNFS.readFile(path,"base64");
        // var file = encrypt(Setting.APPKEY, file);
        // var wfile = await RNFS.writeFile(path, file, 'base64');
    }

    async downloadFile(id, file_id) {

        if (this.state.download != 0) {
            return alertMsg('خطا', 'در حال حاضر دانلود دیگری در جریان است . لطفا منتظر بمانید');
        }
        this.setState({loading: true});
        var res = await Fetcher('checkDownload', {id: file_id}, true);
        this.setState({loading: false, download: 1});
        if (res.status == 'OK') {
            this.setState({download: file_id});
            RNFS.downloadFile({
                fromUrl: res.data,
                toFile: lessonPath + '/' + 'FILE_' + file_id,
                background: true,
                discretionary: true,
                progressDivider: 3,
                progress: this.progress,

            }).promise.then(async (response) => {
                await this.makeVideo(lessonPath + '/' + 'FILE_' + file_id);
                this.setState({download: 0, downloadProgress: 0});
                this.syncFiles(this.state.files);

            });
        } else {
            setTimeout(() => {
                alertMsg('خطا', res.data);
            }, 100);
        }

        // RNFS.writeFile(lessonPath+'/FILE_'+file_id, 'Lorem ipsum dolor sit amet', 'utf8')
        //     .then((success) => {
        //         console.log('FILE WRITTEN!');
        //         this.syncFiles(this.state.files);
        //
        //     })
        //     .catch((err) => {
        //         console.log(err.message);
        //     });
        var res = await RNFS.readDir(lessonPath);
    }

    async checkFile(file_id) {
        var result = await RNFS.exists(lessonPath + '/' + 'FILE_' + file_id);
        return result;
    }

    checkFileRender(file_id) {
        var file = this.state.files.find((res) => {
            return res.id == file_id;
        });
        if (typeof file == 'undefined') {
            return 'nnnnn';
        } else {
            return file.available;
        }

    }

    async deleteFile(file_id) {
        alertMsg(
            'پیام',
            'آیا از حذف این فایل اطمینان دارید ؟ پس از حذف قابل دسترسی نخواهد بود',
            [
                {
                    text: 'خیر',
                    onPress: () => {
                    },
                    style: 'cancel',
                },
                {
                    text: 'حذف فایل', onPress: async () => {
                        await RNFS.unlink(lessonPath + '/' + 'FILE_' + file_id);
                        this.syncFiles(this.state.files);
                    },
                },
            ],
            {cancelable: false},
        );


    }


    changeFav() {
        this.setState({loading: true});
        Fetcher('addFav', {id: this.props.id, type: 'lesson'}, true)
            .then((res) => {
                this.setState({loading: false, liked: !this.state.liked});
            });
    }


    render() {
        var {height, width} = Dimensions.get('window');
        var starsOn = [];
        var starsOff = [];
        for (i = 0; i < parseInt(this.state.stars || 0); i++) {
            starsOn.push(i);
        }
        let remain = 5 - parseInt(this.state.stars || 0);
        for (i = 0; i < parseInt(remain); i++) {
            starsOff.push(i + starsOn.length);
        }

        return (


            <Screen title={this.state.data.title}>

                <Loading new loading={this.state.loading}/>
                <ImageBackground resizeMode={getStore('config').image_scale} source={{uri: this.state.data.pic}}
                                 style={{height: 250}}>
                    {parseFloat(this.state.data.discount) > 0 &&
                    <ImageBackground resizeMode={'stretch'} style={{
                        width: 40,
                        height: 60,
                        top: 0,
                        left: 10,
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                    }} source={require('../assets/images/discountbg.png')}>
                        <FaText style={{
                            bottom: 5,
                            color: Setting.white,
                            fontSize: 14,
                        }}>{Math.round(100 - ((parseFloat(this.state.data.price) - parseFloat(this.state.data.discount)) * 100) / parseFloat(this.state.data.price))}%</FaText>
                    </ImageBackground>
                    }
                </ImageBackground>


                <View style={{
                    marginRight: Setting.contentMargin,
                    marginLeft: Setting.contentMargin,
                    marginTop: 10,
                    marginBottom: 20,
                }}>
                    <FaText border fontSize={18}>{this.state.data.title}</FaText>

                    <FaText html style={{
                        marginTop: 15,
                        marginBottom: 15,
                        textAlign: 'center',
                    }}>{this.state.data.description}</FaText>
                    {!this.state.loading &&
                    <View style={{
                        padding: 5,
                        flexDirection: 'row-reverse',
                        justifyContent: 'space-between',
                        alignItems: 'space-between',
                    }}>
                        {parseFloat(this.state.data.discount) > 0 ?
                            <FaText
                                style={{color: Setting.primaryColor, width: '49%', textDecorationLine: 'line-through'}}
                                textAlign={'right'}>
                                {numberFormat(this.state.data.price)}
                            </FaText>
                            :
                            <View style={{color: Setting.white, width: '49%'}}></View>
                        }
                        <FaText style={{color: Setting.headerColor, width: '49%'}} textAlign={'left'}>
                            {numberFormat(parseFloat(this.state.data.price) - parseFloat(this.state.data.discount))}
                        </FaText>
                    </View>
                    }
                    <View style={{flexDirection: 'row-reverse', justifyContent: 'center'}}>
                        <View style={{width: '100%'}}>
                            {this.state.loading ?
                                <View></View>
                                :
                                this.state.data.owned == '1' ?
                                    <MyButton bg={Setting.green} txtcolor={'#fff'} disable style={{width: '100%'}}>خریداری
                                        شده</MyButton>
                                    :
                                    (this.state.data.for_sale == '1' ?
                                            <MyButton onPress={() => Actions.Payment({
                                                product_name: this.state.data.title,
                                                price: parseFloat(this.state.data.price),
                                                discount: parseFloat(this.state.data.discount),
                                                total: parseFloat(this.state.data.price) - parseFloat(this.state.data.discount),
                                                score: parseFloat(this.state.data.score),
                                                id: this.state.data.id,
                                                typee: 'lesson',
                                            })} style={{width: '100%'}}>خرید این درس</MyButton>
                                            :
                                            <MyButton disable style={{width: '100%'}}>فروش غیرفعال</MyButton>
                                    )
                            }
                            {this.state.data.hasExam == '1' &&
                            <MyButton disable={this.state.data.quizStatus == '1'}
                                      bg={this.state.data.quizStatus == '1' ? Setting.ccc : Setting.green}
                                      txtcolor={'#fff'}
                                      onPress={() => Actions.Exam({id: this.state.data.id, typee: 'lesson'})}
                                      style={{marginTop: 15, marginBottom: 15}}>شرکت در آزمون</MyButton>
                            }
                            <View
                                style={{marginTop: 15, flexDirection: 'row-reverse', justifyContent: 'space-between'}}>
                                <TouchableOpacity onPress={() => this.changeFav()}>
                                    {this.state.liked ?
                                        <Icon name={'heart'} style={{color: Setting.primaryColor}}
                                              type={'FontAwesome'}/>
                                        :
                                        <Icon name={'heart-o'} style={{color: Setting.primaryColor}}
                                              type={'FontAwesome'}/>
                                    }
                                </TouchableOpacity>
                                <View style={{flexDirection: 'row'}}>
                                    {starsOn.map((val, i) => {
                                        return (<TouchableOpacity onPress={() => this.setStar(val)}><Icon name={'star'}
                                                                                                          style={{color: '#fac917'}}
                                                                                                          type={'FontAwesome'}/></TouchableOpacity>);
                                    })}
                                    {starsOff.map((val, i) => {
                                        return (
                                            <TouchableOpacity onPress={() => this.setStar(val)}><Icon name={'star-o'}
                                                                                                      style={{color: '#999'}}
                                                                                                      type={'FontAwesome'}/></TouchableOpacity>);
                                    })}

                                </View>
                            </View>
                            {this.state.data.quizStatus == '1' &&
                            <FaText textAlign={'center'}>شما در آزمون این درس شرکت کرده اید و با کسب
                                امتیاز {this.state.data.quizScore} قبول شده اید .</FaText>
                            }
                        </View>
                    </View>
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Image source={require('../assets/images/book.png')}
                               style={{width: 50, height: 50, marginTop: 20}}/>
                    </View>
                    <FaText fontSize={14} style={{marginTop: 10, marginBottom: 15, textAlign: 'center'}}>فایل
                        ها</FaText>

                    {this.state.files.length > 0 &&
                    <FlatList
                        style={Setting.flatListStyle}
                        data={this.state.files}
                        extraData={this.state}
                        renderItem={({item}) => {

                            return (
                                <TouchableOpacity activeOpacity={1}
                                                  onPress={() => this.processFile(this.props.id, item.id + '.lpg', item.ext, item.type)}
                                                  style={{
                                                      padding: 15,
                                                      borderRadius: Setting.borderRadius,
                                                      marginTop: 10,
                                                      flexDirection: 'row-reverse',
                                                      alignItems: 'center',
                                                      backgroundColor: Setting.f9,
                                                  }}>
                                    <View style={{
                                        flexDirection: 'row-reverse',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        flex: 1,
                                    }}>
                                        <View>
                                            <FaText style={{color: '#7f8aab'}}>{item.title}</FaText>
                                        </View>
                                        {parseInt(this.state.download) == parseInt(item.id) &&
                                        <View style={{justifyContent: 'center'}}>

                                            <FaText style={{
                                                color: '#7f8aab',
                                                fontSize: 10,
                                            }}> {this.state.downloadProgress} % </FaText>
                                            <FaText style={{
                                                color: '#7f8aab',
                                                fontSize: 10,
                                            }}>{this.state.downloadedSize} از {this.state.downloadSize} MB </FaText>
                                        </View>
                                        }
                                        {item.avail == 'yes' ?
                                            <TouchableOpacity style={{
                                                width: 30,
                                                height: 30,
                                                marginLeft: 15,
                                                backgroundColor: Setting.red,
                                                borderRadius: 15,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }} activeOpacity={1} onPress={() => {
                                                this.deleteFile(item.id + '.lpg');
                                            }}>
                                                <Icon style={{color: '#fff', fontSize: 20}} name={'trash'}/>
                                            </TouchableOpacity>
                                            :
                                            <Image source={require('../assets/images/dl.png')}
                                                   style={{width: 30, height: 30, marginLeft: 15}}/>
                                        }

                                    </View>

                                </TouchableOpacity>
                            );
                        }
                        }
                        keyExtractor={(item) => item.id}
                    />
                    }


                </View>


            </Screen>
        );

    }

    setStar(star) {
        alertMsg('ثبت امتیاز', 'آیا مطمین به ذخیره امتیاز هستید ؟', [
            {
                text: 'لغو', onPress: () => {
                },
            },
            {
                text: 'تایید', onPress: () => {
                    let realStar = star + 1;
                    this.setState({stars: realStar});
                    Fetcher('setStar', {type: 'lesson', id: this.state.data.id, star: realStar}, true);
                },
            },
        ]);

    }

}
