import React from 'react';
import {Image, View, PushNotificationIOS, StatusBar, Platform, Dimensions,AsyncStorage} from 'react-native';
import {Spinner} from 'native-base';
import {Actions} from 'react-native-router-flux';
import FaText from "./components/FaText";
import {Fetcher} from "./components/Upload";
import {Setting} from "./components/Setting";
import {setStore} from "trim-redux";
import LinearGradient from 'react-native-linear-gradient';



export default class Splash extends React.Component {

    componentDidMount() {
        // SplashScreen.close({
        //     animationType: SplashScreen.animationType.scale,
        //     duration: 850,
        //     delay: 500,
        // })
        AsyncStorage.removeItem('is_first_time');
    }

    componentWillMount() {
        this.loadData();
        if (Platform.OS != 'ios') {
            StatusBar.setBackgroundColor('#393939');
            StatusBar.setBarStyle('light-content');
        } else {
            StatusBar.setBarStyle('dark-content');
        }
    }

    async loadData() {
        Fetcher("getConfig", {})
            .then((res) => {
                if (res.status == 'OK') {
                    setStore("config", res.data);
                }
            });
        console.log("loadData");
        Fetcher("GetMain", {}, true)
            .then(async (res) => {
                console.log('get main',res);

                if (res.status == 'OK') {
                    setStore({
                        homeData: res.data,
                        unreadMsg: parseInt(res.data.user.unread_msg),
                    });
                    console.log('new res',res.data)
                    AsyncStorage.setItem('file_names', JSON.stringify(res.data.file_names));
                    const isFirstTime = await AsyncStorage.getItem('is_first_time');
                    if (isFirstTime !== null) {
                        Actions.reset("main");
                    } else {
                        Actions.intro();
                        await AsyncStorage.setItem('is_first_time', '1')
                    }

                } else {
                    AsyncStorage.removeItem("token")
                        .then(() => {
                            Actions.reset("login");
                        });
                }
            });

    }

    render() {
        var {height, width} = Dimensions.get('window');

        if (this.props.soft === undefined)
            return (
                <LinearGradient colors={['#f35e66', '#f06b3d']} style={{flex: 1, alignItems: 'center'}}>
                    <View style={{marginTop: '15%'}}>
                        <Image resizeMode={'stretch'} style={{width: width, height: width / 2.024}}
                               source={require('./../assets/images/logo_two.png')}/>
                        <FaText style={{color: '#fff', fontSize: 25, textAlign: 'center', marginTop: 10}}>تصادف
                            میمیراند...</FaText>
                    </View>
                    <View style={{position: 'absolute', bottom: height / 10, alignItems: 'center'}}>
                        <Image resizeMode={'contain'} style={{width: 50, height: 50}}
                               source={require('./../assets/images/loading.gif')}/>

                        <FaText style={{fontSize: 10, color: Setting.white, marginTop: height / 12}}>نسخه 2.2.0</FaText>
                    </View>
                </LinearGradient>
            );
        else
            return (
                <LinearGradient colors={['#fff', '#fff']} style={{flex: 1, alignItems: 'center'}}>
                    <View style={{position: 'absolute', bottom: 15, alignItems: 'center', marginTop: '60%'}}>
                        <Spinner color="black"/>
                    </View>
                </LinearGradient>
            );

    }


}
