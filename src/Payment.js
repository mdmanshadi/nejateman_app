import React from 'react';
import {View, ScrollView, Dimensions, Image, FlatList, TouchableOpacity, Alert} from 'react-native';
import {connect} from 'trim-redux';
import FaText from './components/FaText';
import {Setting, numberFormat} from './components/Setting';
import Loading from './components/Loading';
import {Fetcher} from './components/Upload';
import MyButton from './components/MyButton';
import Communications from 'react-native-communications';
import TextField from './components/TextField';
import Screen from './components/Screen';
import {alertMsg, toastMsg} from './components/Helper';

class Payment extends React.Component {

    componentWillMount() {
        var type = '';
        if (this.props.typee == 'package') {
            type = 'پکیج';
        } else if (this.props.typee == 'course') {
            type = 'دوره';
        } else if (this.props.typee == 'lesson') {
            type = 'درس';
        }

        this.setState({
            loading: true,
            couponSet: false,
            type: type,
            code: '',
            couponPercent: 0,
            total: this.props.total,
            discount: this.props.discount,
            different: 0,
        });
        Fetcher('getDifferentPrice',
            {
                id: this.props.id,
                type: this.props.typee,
            }
            , true).then((res) => {
            if (res.status === 'OK') {
                this.setState({different: res.data, loading: false});
            } else {
                this.setState({loading: false});
            }

        }).catch(() => this.setState({loading: false}));

    }

    initPay() {
        if (this.props.homeData.user.credit > 0) {
            return alertMsg(
                'پرداخت از کیف پول',
                'موجودی کیف پول شما ' + this.props.homeData.user.credit_text + '(ممکن است در صورت کمبود موجودی به درگاه پرداخت هدایت شوید)  است \nآیا تمایل دارید پرداخت از کیف پول شما انجام شود ؟',
                [
                    {text: 'پرداخت از کیف پول', onPress: () => this.startPay(1)},
                    {
                        text: 'پرداخت آنلاین',
                        onPress: () => this.startPay(2),
                        style: 'cancel',
                    },
                ],
                {cancelable: true},
            );
        } else {
            return this.startPay(2);
        }
    }

    async startPay(wallet) {
        this.setState({loading: true});
        var res = await Fetcher('startPay', {
            id: this.props.id,
            wallet: wallet,
            type: this.props.typee,
            code: this.state.code,
            different: this.state.different
        }, true);
        this.setState({loading: false});
        console.log('response',res);
        if (res.status == 'OK') {
            if ((parseInt(this.props.homeData.user.credit) >= parseInt(this.state.total)) && wallet == '1') {
                fetch(res.data)
                    .then((r) => {
                        Communications.web('nejateman://App/1');
                    });
            } else {
                Communications.web(res.data);
            }

        } else {
            setTimeout(() => toastMsg(res.data), 100);
        }
    }

    setCoupon = async () => {
        if (this.state.code == '') {
            return alertMsg('خطا', 'لطفا کد تخفیف خود را وارد کنید');
        }
        this.setState({loading: true});
        var res = await Fetcher('checkCoupon', {
            id: this.props.id,
            type: this.props.typee,
            code: this.state.code,
        }, true);
        this.setState({loading: false});
        if (res.status == 'OK') {
            this.setState({
                couponSet: true,
                couponPercent: (res.data.percent),
                discount: parseFloat(res.data.discount),
                total: parseFloat(res.data.total),
            });

        } else {
            setTimeout(() => toastMsg(res.data), 100);
        }

    };

    render() {

        return (

            <Screen title={'خرید ' + this.state.type}>
                <Loading loading={this.state.loading}/>
                <View style={{
                    marginRight: 5,
                    marginLeft: 5,
                    marginTop: 5,
                    marginBottom: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>

                    <View style={{flexDirection: 'row-reverse', justifyContent: 'space-between', margin: 5}}>
                        <TextField slim padding={{paddingTop: 3, paddingBottom: 3}} style={{
                            width: '75%',
                            padding: 25,
                            marginLeft: 10,
                            backgroundColor: '#f4f4f4',
                            borderColor: '#f4f4f4',
                        }} fontSize={9} placeholder={'کد تخفیف دارید ؟'} value={this.state.code}
                                   onChangeText={(value) => this.setState({code: value})}/>
                        <MyButton slim style={{width: '18%'}} cntstyle={{margin: 5}}
                                  padding={{paddingTop: 8, paddingBottom: 8}} fontSize={10}
                                  onPress={() => this.setCoupon()}>اعمال کد</MyButton>
                    </View>

                    <View style={{
                        margin: 5,
                        marginTop: 30,
                        borderColor: Setting.white,
                        padding: 20,
                        width: '95%',
                        borderRadius: Setting.borderRadius,
                        flexDirection: 'column',
                    }}>

                        <View style={{
                            borderColor: '#d0d5e7',
                            borderWidth: 1,
                            flexDirection: 'row-reverse',
                            alignItems: 'center',
                            margin: 5,
                            justifyContent: 'space-between',
                            borderRadius: Setting.borderRadius,
                            padding: 10,
                        }}>
                            <FaText>محصول انتخابی : </FaText>
                            <FaText marquee style={{
                                width: '50%',
                                color: Setting.primaryColor,
                            }}>{this.props.product_name}</FaText>
                        </View>
                        <View style={{
                            borderColor: '#d0d5e7',
                            borderWidth: 1,
                            flexDirection: 'row-reverse',
                            alignItems: 'center',
                            margin: 5,
                            justifyContent: 'space-between',
                            borderRadius: Setting.borderRadius,
                            padding: 10,
                        }}>
                            <FaText>امتیاز دریافتی پس از خرید : </FaText>
                            <FaText style={{color: Setting.primaryColor}}>{numberFormat(this.props.score)}</FaText>
                        </View>
                        <View style={{
                            borderColor: '#d0d5e7',
                            borderWidth: 1,
                            flexDirection: 'row-reverse',
                            alignItems: 'center',
                            margin: 5,
                            justifyContent: 'space-between',
                            borderRadius: Setting.borderRadius,
                            padding: 10,
                        }}>
                            <FaText>مبلغ کل : </FaText>
                            <FaText style={{color: Setting.primaryColor}}>{numberFormat(this.props.price)}</FaText>
                        </View>

                        <View style={{
                            borderColor: '#d0d5e7',
                            borderWidth: 1,
                            flexDirection: 'row-reverse',
                            alignItems: 'center',
                            margin: 5,
                            justifyContent: 'space-between',
                            borderRadius: Setting.borderRadius,
                            padding: 10,
                        }}>
                            <FaText>تخفیف : </FaText>
                            <FaText style={{color: Setting.primaryColor}}>{numberFormat(this.state.discount)}</FaText>
                        </View>
                        <View style={{
                            borderColor: '#d0d5e7',
                            borderWidth: 1,
                            flexDirection: 'row-reverse',
                            alignItems: 'center',
                            margin: 5,
                            justifyContent: 'space-between',
                            borderRadius: Setting.borderRadius,
                            padding: 10,
                        }}>
                            <FaText>تخفیف خرید جزئی قبلی : </FaText>
                            <FaText style={{color: Setting.primaryColor}}>{numberFormat(this.state.different)}</FaText>
                        </View>
                        <View style={{
                            borderColor: '#d0d5e7',
                            borderWidth: 1,
                            flexDirection: 'row-reverse',
                            alignItems: 'center',
                            margin: 5,
                            justifyContent: 'space-between',
                            borderRadius: Setting.borderRadius,
                            padding: 10,
                        }}>
                            <FaText>مبلغ قابل پرداخت : </FaText>
                            <FaText style={{color: Setting.primaryColor}}>{numberFormat(this.state.total-this.state.different)}</FaText>
                        </View>


                        <MyButton onPress={() => this.initPay()} style={{marginTop: 50}}>ادامه</MyButton>

                    </View>


                </View>


            </Screen>
        );

    }

}

const mapStateToProps = (state) => {
    return {
        homeData: state.homeData

    }
};
export default connect(mapStateToProps)(Payment);

