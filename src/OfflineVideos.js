import React from 'react';
import {View, TouchableOpacity, FlatList, Dimensions, AsyncStorage} from 'react-native';
import Loading from "./components/Loading";
import FaText from "./components/FaText";
import {Setting} from "./components/Setting";
import {Actions} from "react-native-router-flux";
import Screen from "./components/Screen";


var RNFS = require('react-native-fs');
var filesPath = RNFS.DocumentDirectoryPath + '/files';
var lessonPath = filesPath;


export default class OfflineVideos extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {

        this.setState({
            files: [],
            loading: true
        });
        this.getFilesInformation();
    }

    async getFilesInformation() {
        let fileNames = await AsyncStorage.getItem('file_names');
        let finalFileNames = JSON.parse(fileNames);
        console.log('file names',finalFileNames);
        let directories = await RNFS.readDir(lessonPath);
        directories.forEach(async (directory) => {
            let dirFiles = await RNFS.readDir(directory.path);
            console.log('direFiles',dirFiles);
            if (dirFiles.length !== 0) {
                dirFiles.forEach((file) => {
                    let fileName = file.name.split('_')[1].split('.')[0];


                    let finalFileName = finalFileNames[fileName];
                    console.log('fileName',finalFileName);
                    this.setState({
                        files: [...this.state.files , {name: finalFileName, path: file.path}],
                    })
                });

            }
        });

        this.setState({
            loading: false,
        })
    }

    render() {
        var {height, width} = Dimensions.get('window');
        return (
            <Screen title='مشاهده آفلاین ویدیوها' nomenu={true}>
                <Loading new loading={this.state.loading}/>
                {this.state.files.length !== 0   ? <FlatList
                    style={Setting.flatListStyle}
                    data={this.state.files}
                    renderItem={({item}) => {
                        return (
                            <TouchableOpacity activeOpacity={1}
                                              onPress={() => {
                                                 return Actions.DupPlayer({path: item.path, ext: 'm4v', typee: 'video'});
                                              }}
                                              style={{
                                                  padding: 15,
                                                  borderRadius: Setting.borderRadius,
                                                  marginTop: 10,
                                                  alignItems: 'center',
                                                  backgroundColor: Setting.f9,
                                              }}>
                                <View>
                                    <FaText style={{color: '#7f8aab',textAlign: 'right'}}>{item.name}</FaText>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                    keyExtractor={(item) => item.name}
                /> : <FaText style={{color: '#7f8aab',textAlign: 'center'}}>{this.state.loading === false ? 'تاکنون هیچ فایلی دانلود نشده است':''}</FaText> }
            </Screen>
        );
    }
}


