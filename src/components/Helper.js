import {Toast} from "native-base";
import {DialogHelper} from "./DialogHelper";

export const toastMsg = (text,type = '',position = 'top',mode='dialog')=>{
    if(mode == 'dialog')
        return  DialogHelper.show(text);
    else
        return Toast.show({
            text: text,
            buttonText: 'باشه',
            type: type,
            position:position,
            duration: 3000,
            textStyle : {
                fontFamily:'IRANSansWeb(FaNum)',
                fontSize:10,
                textAlign:'right'
            },
            buttonTextStyle : {
                fontFamily:'IRANSansWeb(FaNum)',
                fontSize:10,
            }
        })
};
export const alertMsg = (title,text,buttons)=>{
    return  DialogHelper.show(title+'\n\n'+text,buttons);
};