import React, { Component } from 'react';
import { TextInput,StyleSheet} from 'react-native';
import {Setting} from "./Setting";
import RNPickerSelect from "./RNPickerSelect";

export default class Pick extends Component {
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <RNPickerSelect
                onValueChange={this.props.onValueChange}
                items={this.props.items}
                value={this.props.value}
                placeholder={{ label: this.props.placeholder || 'انتخاب کنید', value: '0' }}
                style={pickerSelectStyles}
                useNativeAndroidPickerStyle={false}

            />
        );
    }
}
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        width:'100%',
        marginTop:5,
        marginBottom:5,
        padding:10,

        textAlign:'right',
        borderWidth:1,
        borderRadius:Setting.borderRadius,
        borderColor:'#cacfe0',
        color:'#000',
        // lineHeight:1,
        fontFamily:'IRANSansWeb(FaNum)',
        fontSize:11,

    },
    inputAndroid: {
        width:'100%',
        color:'#000',
        marginTop:5,
        marginBottom:5,

        textAlign:'right',
        borderWidth:1,
        borderRadius:Setting.borderRadius,
        borderColor:'#cacfe0',
        // lineHeight:1,
        fontFamily:'IRANSansWeb(FaNum)',
        fontSize:11,

    },
});

