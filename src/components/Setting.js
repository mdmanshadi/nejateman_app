export const Setting = {
    APPKEY : 'OghiyaNoosEAbi1234567890',
    BASEURL : 'https://nejatman.liara.run/api/1/Base/',
    // BASEURL : 'http://10.0.2.2/bp-app-web/public/api/1/Base/',
    // BASEURL : 'http://192.168.1.3/bp-app-web/public/api/1/Base/',
    primaryColor : '#f26c5c',
    secondColor : '#0072bc',
    ccc : '#535353',
    thirdColor : '#e6e7e8',
    headerColor : '#092061',
    white : '#fff',
    red : '#f26c5c',
    green : '#f26c5c',
    success : '#33691E',
    f3 : '#f3f3f7',
    f9 : '#f9f9f9',
    grey : '#c2c2c2',
    contentMargin : 50,
    borderRadius : 7,
    bg : '#fff',
    sliderStyle : {
        width: '100%',
        height :200,
    },
    flatListStyle : {
        marginTop:15,
        marginBottom:15
    }
};
export function numberFormat(x) {
    if(typeof x == 'undefined')
        return "";
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' تومان';
}
