import React, { Component } from 'react';
import { TouchableOpacity, View, Modal, Dimensions} from 'react-native';
import {StyleProvider, Container, Content,Icon} from 'native-base';
import FaText from "./FaText";
import {Actions} from "react-native-router-flux";
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import Loading from "./Loading";
import Header from "./Header";
import {Setting} from "./Setting";

export default class Screen extends Component {
    close(){
        if(this.props.iconAction === undefined)
            return Actions.popTo("home");
        else return this.props.iconAction(false);
    }
    render() {
        var {height, width} = Dimensions.get('window');


        return (
            <StyleProvider style={getTheme(material)}>
                <Container style={{backgroundColor:Setting.bg}}>

                    <Header nomenu={this.props.nomenu} float={this.props.float} title={this.props.title} home={this.props.home}/>
                    <Content style={{padding:this.props.padding === undefined ? 0 : this.props.padding,flex:1}}>
                        {this.props.children}
                        <View style={{alignItems:'center',marginBottom:50}}>
                        </View>
                    </Content>
                </Container>
            </StyleProvider>
        );
    }
}