import React, { Component } from 'react';
import {View, Text, Image, TouchableOpacity, Dimensions} from 'react-native';
import {Icon} from "native-base";
import {Actions} from "react-native-router-flux";
import { Platform } from "react-native";
import FaText from "./FaText";
import {Setting} from "./Setting";


export default class Header extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        var {height, width} = Dimensions.get('window');
        var float = {justifyContent:'center',flexDirection:'row-reverse',alignItems:'center',width:width,top:(Platform.OS == 'ios' ? 25 : 10),position:'absolute',paddingTop:0,marginTop:0,zIndex:999};
        var fixed = {justifyContent:'center',flexDirection:'row-reverse',alignItems:'center',width:width,paddingBottom:5,paddingTop: (Platform.OS == 'ios' ? 25 : 10)};


        return (
            <View style={this.props.float ? float : fixed}>
                <View style={{
                    width:width -20,
                    backgroundColor:Setting.headerColor,
                    paddingRight:10,
                    paddingLeft:10,
                    flexDirection:'row-reverse',
                    alignItems:'center',
                    padding:1,
                    justifyContent:'space-between',
                    borderRadius:Setting.borderRadius,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,

                    elevation: 4,

                }}>
                    {this.props.nomenu ?
                        <View></View>
                        :
                        <TouchableOpacity  activeOpacity={1} style={{padding:0}} onPress={()=>Actions.drawerOpen()} style={{width:'10%'}}><Icon style={{color:'#fff',padding:0}} name={'ios-menu'}  /></TouchableOpacity>
                    }
                    {this.props.title ?
                            <FaText marquee style={{color:'#fff'}}>{this.props.title}</FaText>
                        :
                        <Image resizeMode={'stretch'} source={require('./../../assets/images/minlogo.png')} style={{width:60,height:(44/150) * 60}} />

                    }
                    {this.props.home ?
                        <View></View>
                        :
                        <TouchableOpacity  activeOpacity={1} onPress={()=>Actions.pop()} style={{width:'10%'}}>
                            <View style={{padding:0}}><Image source={require('../../assets/images/left-arrow.png')} style={{width:30,height:30*(36/52)}} /></View>
                        </TouchableOpacity>

                    }
                </View>
            </View>

        );
    }
}

