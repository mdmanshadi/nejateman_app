import React, { Component } from 'react';
import {View,TouchableOpacity} from 'react-native';
import FaText from "./FaText";
import {Setting} from "./Setting";
import LinearGradient from 'react-native-linear-gradient';

export default class MyButton extends Component {
    render() {
        let THEView = LinearGradient;
        if(this.props.gradient)
            THEView = LinearGradient;

        let colors = [ '#f06b3d','#f35e66'];

        let padding = {
            paddingTop:(this.props.slim ? 5 :15),
            paddingBottom:(this.props.slim ? 5 :15),
            paddingRight:(this.props.slim ? 15 :20),
            paddingLeft:(this.props.slim ? 15 :20),
        };
        if(this.props.padding)
            padding = this.props.padding;

        return (
            <TouchableOpacity  activeOpacity={1} onPress={()=>{
                if(!this.props.disable)
                    return this.props.onPress();
            }} style={[{justifyContent:'center',alignItems:'center',width:'100%'},this.props.style]}>
            <THEView colors={colors} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={[{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.23,
                shadowRadius: 2.62,
                elevation: 2,
                width:'100%',



                alignItems:'center',
                backgroundColor:((typeof this.props.bg == 'undefined') ? (this.props.disable ? Setting.thirdColor:Setting.primaryColor) : this.props.bg),borderRadius:Setting.borderRadius},this.props.cntstyle,padding]}>
                {this.props.view === undefined ?
                    <FaText
                        style={{color: ((typeof this.props.txtcolor == 'undefined') ? (this.props.disable ? Setting.white : Setting.white) : this.props.txtcolor),
                            fontSize: ((typeof this.props.fontSize == 'undefined') ?  15 : this.props.fontSize)
                        }}>{this.props.children}</FaText>
                    :
                    <View>{this.props.children}</View>
                }
            </THEView>
            </TouchableOpacity>
        );
    }
}