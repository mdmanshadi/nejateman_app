import { Actions } from 'react-native-router-flux';
import {AsyncStorage,Platform,Alert} from 'react-native';
import {Setting} from "./Setting";
const DEBUG = true;

export  const Fetcher = async (func,data,loginRequired,upload = false) =>{

    if(loginRequired) {
        var token = await AsyncStorage.getItem("token");
        var notifToken = await AsyncStorage.getItem("notifToken");
        if(token === undefined || token == null || token == '')
            return Actions.reset("login");
        if(upload) {
           data.append('token',token);
           data.append('notifToken',notifToken);
        }else{
            data['token'] = token;
            data['notifToken'] = notifToken;
        }
    }
    if(upload){
        data.append('version','100');
        data.append('platform','Nejateman|'+Platform.OS+'|'+Platform.Version);
    }else{
        data['version'] = '100';
        data['platform'] = 'Nejateman|'+Platform.OS+'|'+Platform.Version;
    }


    if(DEBUG) {
        console.log("Func :", func);
        console.log("Input :", data);
    }
    try {
        var res = await fetch(Setting.BASEURL + func, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': upload ? 'multipart/form-data' : 'application/json',
            },
            body: upload ? data : JSON.stringify(data),
        });
        var out = await res.json();


        if(out.status == 'error' && out.data == 'Not Logged In'){
            if(DEBUG)
                console.log("logged out");
            AsyncStorage.removeItem("token");
            return Actions.reset("login");
        }
        if(DEBUG)
            console.log("Output :",out);
        return out;
    }catch (e) {
        if(DEBUG)
            console.log("Error :",e);
        return Actions.replace('NoNet',{error:func,errordesc:JSON.stringify(data)});
    }
}
